//
//  AcabadoViewController.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 28/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AcabadoViewController.h"
#import "JMCellStyleValue1.h"
#import "JMCellStyleDefault.h"
#import "JMFactoresMinoracion.h"
#import "Acabados.h"

// START:TableSections
enum JMTableSections {
    JMTableSectionFichaTecnica = 0,
    JMTableSectionPrecios,
    JMTableNumSections,
};
// END:TableSections

// START:TableRows
enum JMFichaTecnicaElapsedRows {
    JMTableSecFichaRowPeriodoComercial = 0,
    JMTableSecFichaRowValorNuevo,
    JMTableSecFichaRowCaballosVapor,
    JMTableSecFichaRowTitulo,
    JMTableSecFichaRowCilindrada,
    JMTableSecFichaRowCilindros,
    JMTableSecFichaRowTipoCombustible,
    JMTableSecFichaRowPotenciaKWh,
    JMTableSecFichaRowCaballosFiscales,
    JMTableSecFichaRowEmisionCO2,
    JMTableSecFichaNumRows,
};
// START:TableRows
enum JMFichaTecnicaCollapsedRows {
    JMTableSecCollapsedFichaRowPeriodoComercial = 0,
    JMTableSecCollapsedFichaRowValorNuevo,
    JMTableSecCollapsedFichaRowCaballosVapor,
    JMTableSecCollapsedFichaRowTitulo,
    JMTableSecCollapsedFichaNumRows,
};

enum JMPreciosCollapsedRows {
    JMTableSecCollapsedPreciosTitulo = 0,
    JMTableSecCollapsedPreciosNumRows,
};


@implementation AcabadoViewController

@synthesize acabado;
@synthesize arrayAcabado;
@synthesize disclosureButton;
@synthesize disclosureButtonPrecios;
@synthesize collapsedFicha;
@synthesize collapsedPrecios;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    self.title = NSLocalizedString(@"Modelo - Tipo", @"Modelo - Tipo");
    self.tableView.backgroundColor = [UIColor underPageBackgroundColor];
    
    arrayAcabado = [JMFactoresMinoracion valoresVenalesSucesivosParaValorDeReferencia:[acabado valueForKey:@"valorAcabado"]];
    
    collapsedFicha = YES;
    collapsedPrecios = YES;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return JMTableNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // START:NumRows
    switch (section) {
        case JMTableSectionFichaTecnica:
            if (collapsedFicha) {
                return JMTableSecCollapsedFichaNumRows;
            }
            return JMTableSecFichaNumRows;
        case JMTableSectionPrecios:
            if (collapsedPrecios) {
                return JMTableSecCollapsedPreciosNumRows;
            }
            return JMTableSecCollapsedPreciosNumRows + [arrayAcabado count];
        default:
            NSLog(@"Unexpected section (%d)", section);
            break;
    }
    // END:NumRows
    return 0;
}

- (NSString *)marcaYModelo
{
    NSManagedObject *marca = (NSManagedObject *)[acabado valueForKey:@"marca"];
    NSString *marcaString = [marca valueForKey:@"marca"];
    NSString *acabadoString = [acabado valueForKey:@"acabado"];
    return [NSString stringWithFormat:@"%@ - %@", marcaString, acabadoString];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == JMTableSectionFichaTecnica) {
        return [self marcaYModelo];
    }
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    cell = [JMCellStyleValue1 cellForTableView:tableView];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.indentationLevel = 0;

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    // START:SearchingSection
    switch (indexPath.section) {
        case JMTableSectionFichaTecnica:
            if (indexPath.row < JMTableSecCollapsedFichaNumRows) {
                switch (indexPath.row) {
                    case JMTableSecCollapsedFichaRowPeriodoComercial:
                        cell.textLabel.text = NSLocalizedString(@"Periodo Comercial", @"Periodo Comercial");
                        cell.detailTextLabel.text = [acabado valueForKey:@"periodoComercial"];
                        break;
                    case JMTableSecCollapsedFichaRowCaballosVapor:
                        cell.textLabel.text = NSLocalizedString(@"Caballos de vapor", @"Caballos de vapor");
                        cell.detailTextLabel.text = [acabado valueForKey:@"caballosVapor"];
                        break;
                    case JMTableSecCollapsedFichaRowValorNuevo:
                        cell.textLabel.text = NSLocalizedString(@"Valor referencia", @"Valor referencia");
                        cell.detailTextLabel.text = [numberFormatter stringFromNumber:[acabado valueForKey:@"valorAcabado"]];
                        break;
                    case JMTableSecCollapsedFichaRowTitulo: {
                        // Create and configure the disclosure button.
                        cell = [JMCellStyleDefault cellForTableView:tableView];
                        cell.selectionStyle = UITableViewCellSelectionStyleGray;
                        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                        button.frame = CGRectMake(0.0, 5.0, 35.0, 35.0);
                        [button setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
                        [button setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateSelected];
                        [button addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                        disclosureButton = button;
                        cell.accessoryView = disclosureButton;
                        
//                        if (collapsedFicha) {
//                            cell.textLabel.text = NSLocalizedString(@"Más datos", @"Más datos");
//                        } else {
//                            cell.textLabel.text = NSLocalizedString(@"Menos datos", @"Menos datos");
//                        }
                        cell.textLabel.text = NSLocalizedString(@"Más datos", @"Más datos");
                        break;
                    }
                    default:
                        NSAssert1(NO, @"Unexpected row in Colapsed section: %d",
                                  indexPath.row);
                        break;
                }
            }
            if (!collapsedFicha && indexPath.row >= JMTableSecCollapsedFichaNumRows) {
                switch (indexPath.row) {
                    case JMTableSecFichaRowCilindrada:
                        cell.textLabel.text = NSLocalizedString(@"Cilindrada", @"Cilindrada");
                        cell.detailTextLabel.text = [acabado valueForKey:@"cubicaje"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowCilindros:
                        cell.textLabel.text = NSLocalizedString(@"Número de cilindros", @"Número de cilindros");
                        cell.detailTextLabel.text = [acabado valueForKey:@"cilindros"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowTipoCombustible:
                        cell.textLabel.text = NSLocalizedString(@"Tipo de combustible", @"Tipo de combustible");
                        cell.detailTextLabel.text = [acabado valueForKey:@"tipoCombustible"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowPotenciaKWh:
                        cell.textLabel.text = NSLocalizedString(@"Potencia en KW/h", @"Potencia en KW/h");
                        cell.detailTextLabel.text = [acabado valueForKey:@"potenciaKWH"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowCaballosFiscales:
                        cell.textLabel.text = NSLocalizedString(@"Caballos fiscales", @"Caballos fiscales");
                        cell.detailTextLabel.text = [acabado valueForKey:@"caballosFiscales"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowEmisionCO2:
                        cell.textLabel.text = NSLocalizedString(@"Emisión CO2", @"Emisión CO2");
                        cell.detailTextLabel.text = [acabado valueForKey:@"emisionesCO2"];
                        cell.indentationLevel = 1;
                        break;
                    default:
                        NSAssert1(NO, @"Unexpected row in Elapsed section: %d",
                                  indexPath.row);
                        break;
                }
            }
            break;
        case JMTableSectionPrecios:
            if (indexPath.row < JMTableSecCollapsedPreciosNumRows) {
                cell = [JMCellStyleDefault cellForTableView:tableView];
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = CGRectMake(0.0, 5.0, 35.0, 35.0);
                [button setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
                [button setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateSelected];
                [button addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                disclosureButtonPrecios = button;
                cell.accessoryView = disclosureButtonPrecios;
                
                cell.textLabel.text = NSLocalizedString(@"Precio por años de uso", @"Precio por años de uso");
            } else {
                int desde = 0;
                int hasta = 0;
                switch (indexPath.row - 1) {
                    case 0:
                        cell.textLabel.text = [NSString stringWithFormat:@"Menos de un año"];
                        break;
                        
                    case 12:
                        cell.textLabel.text = [NSString stringWithFormat:@"Más de 12 años"];
                        break;
                        
                    default:
                        desde = [[[arrayAcabado objectAtIndex:indexPath.row - 1] valueForKey:@"desde"] intValue];
                        hasta = [[[arrayAcabado objectAtIndex:indexPath.row - 1] valueForKey:@"hasta"] intValue];
                        cell.textLabel.text = [NSString stringWithFormat:@"Entre %i y %i años", desde, hasta];
                        break;
                }
                cell.detailTextLabel.text = [numberFormatter stringFromNumber:[[arrayAcabado objectAtIndex:indexPath.row - 1] valueForKey:@"valorVenal"]];
            }
            break;
        default:
            NSAssert1(NO, @"Unexpected section (%d)", indexPath.section);
            break;
            // END:SubmitSection
    }
    
    return cell;
}

- (void)accessoryButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        [self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == JMTableSectionFichaTecnica) {
        if (collapsedFicha) {
            [self openSection];
        } else {
            [self closeSection];
        }
    } else if (indexPath.section == JMTableSectionPrecios) {
        if (collapsedPrecios) {
            [self openSectionPrecios];
        } else {
            [self closeSectionPrecios];
        }
    }
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == JMTableSectionFichaTecnica && indexPath.row == JMTableSecCollapsedFichaRowTitulo && collapsedFicha) {
        [self openSection];
    } else if (indexPath.section == JMTableSectionFichaTecnica && indexPath.row == JMTableSecCollapsedFichaRowTitulo && !collapsedFicha) {
        [self closeSection];
    } else if (indexPath.section == JMTableSectionPrecios && indexPath.row == JMTableSecCollapsedPreciosTitulo && collapsedPrecios) {
        [self openSectionPrecios];
    } else if (indexPath.section == JMTableSectionPrecios && indexPath.row == JMTableSecCollapsedPreciosTitulo && !collapsedPrecios) {
        [self closeSectionPrecios];
    }
}

- (void)openSection
{
    self.disclosureButton.selected = !self.disclosureButton.selected;
    
    collapsedFicha = NO;
    NSInteger countOfRowsToInsert = JMTableSecFichaNumRows;
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = JMTableSecCollapsedFichaNumRows; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:JMTableSectionFichaTecnica]];
    }
    
    // Apply the updates.
    [self.tableView beginUpdates];
//    [self.tableView reloadRowsAtIndexPaths:[NSMutableArray arrayWithObject:[NSIndexPath indexPathForRow:JMTableSecCollapsedFichaRowTitulo inSection:JMTableSectionFichaTecnica]] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:JMTableSecFichaRowEmisionCO2 inSection:JMTableSectionFichaTecnica] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

- (void)closeSection
{
    self.disclosureButton.selected = !self.disclosureButton.selected;
    
    collapsedFicha = YES;
    NSInteger countOfRowsToDelete = JMTableSecFichaNumRows;
    NSMutableArray *indexPathToDelete = [[NSMutableArray alloc] init];
    for (NSInteger i = JMTableSecCollapsedFichaNumRows; i < countOfRowsToDelete; i++) {
        [indexPathToDelete addObject:[NSIndexPath indexPathForRow:i inSection:JMTableSectionFichaTecnica]];
    }
    
    // Apply the updates.
    [self.tableView beginUpdates];
//    [self.tableView reloadRowsAtIndexPaths:[NSMutableArray arrayWithObject:[NSIndexPath indexPathForRow:JMTableSecCollapsedFichaRowTitulo inSection:JMTableSectionFichaTecnica]] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView deleteRowsAtIndexPaths:indexPathToDelete withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
}

- (void)openSectionPrecios
{
    self.disclosureButtonPrecios.selected = !self.disclosureButtonPrecios.selected;
    
    collapsedPrecios = NO;
    NSInteger countOfRowsToInsert = [arrayAcabado count] + JMTableSecCollapsedPreciosNumRows;
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = JMTableSecCollapsedPreciosNumRows; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:JMTableSectionPrecios]];
    }
    
    // Apply the updates.
    [self.tableView beginUpdates];
//    [self.tableView reloadRowsAtIndexPaths:[NSMutableArray arrayWithObject:[NSIndexPath indexPathForRow:JMTableSecCollapsedPreciosTitulo inSection:JMTableSectionPrecios]] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:JMTableSecCollapsedPreciosTitulo inSection:JMTableSectionPrecios] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

- (void)closeSectionPrecios
{
    self.disclosureButtonPrecios.selected = !self.disclosureButtonPrecios.selected;
    
    collapsedPrecios = YES;
    NSInteger countOfRowsToDelete = [arrayAcabado count] + JMTableSecCollapsedPreciosNumRows;
    NSMutableArray *indexPathToDelete = [[NSMutableArray alloc] init];
    for (NSInteger i = JMTableSecCollapsedPreciosNumRows; i < countOfRowsToDelete; i++) {
        [indexPathToDelete addObject:[NSIndexPath indexPathForRow:i inSection:JMTableSectionPrecios]];
    }
    
    // Apply the updates.
    [self.tableView beginUpdates];
//    [self.tableView reloadRowsAtIndexPaths:[NSMutableArray arrayWithObject:[NSIndexPath indexPathForRow:JMTableSecCollapsedPreciosTitulo inSection:JMTableSectionPrecios]] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView deleteRowsAtIndexPaths:indexPathToDelete withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
}

@end
