//
//  JMFactoresMinoracion.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 31/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMFactoresMinoracion.h"

@implementation JMFactoresMinoracion

+(NSNumber *)valorVenalParaAntiguedad:(NSInteger)years conValorDeReferencia:(NSNumber *)valorReferencia
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FactoresUso" ofType:@"plist"];
    NSMutableArray *factoresUso = [[NSMutableArray alloc] initWithContentsOfFile:path];
    NSArray *filtered = [factoresUso filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(desde <= %i && hasta > %i )", years, years]];
    
    int factor = 0;
    if ([filtered count] > 0) {
        factor = [[[filtered objectAtIndex:0] valueForKey:@"factor"] intValue]; 
    }
    
    float valor = [valorReferencia floatValue];
    float valorVenal = valor * factor / 100;
    
    return [NSNumber numberWithFloat:valorVenal];    

}

+(NSMutableArray *)valoresVenalesSucesivosParaValorDeReferencia:(NSNumber *)valorReferencia
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FactoresUso" ofType:@"plist"];
    NSMutableArray *factoresUso = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    int factor = 0;
    float valor = [valorReferencia floatValue];
    float valorVenal = 0;
    
    NSMutableDictionary *mutableDictonary = nil;
    for (int i = 0; i < [factoresUso count]; i++) {
        mutableDictonary = (NSMutableDictionary *)[factoresUso objectAtIndex:i];
        factor = [[mutableDictonary valueForKey:@"factor"] intValue]; 
        valorVenal = valor * factor / 100;
        [mutableDictonary setObject:[NSNumber numberWithFloat:valorVenal] forKey:@"valorVenal"];
        [factoresUso replaceObjectAtIndex:i withObject:mutableDictonary];
    }
    
    return factoresUso;    
        
}

@end
