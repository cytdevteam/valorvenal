//
//  AcabadoViewController.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 28/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcabadoViewController : UITableViewController

@property (nonatomic, strong) NSManagedObject *acabado;
@property (nonatomic, strong) NSMutableArray *arrayAcabado;
@property (nonatomic, strong) UIButton *disclosureButton;
@property (nonatomic, strong) UIButton *disclosureButtonPrecios;
@property BOOL collapsedFicha;
@property BOOL collapsedPrecios;

- (void)openSection;
- (void)closeSection;
- (void)openSectionPrecios;
- (void)closeSectionPrecios;

@end
