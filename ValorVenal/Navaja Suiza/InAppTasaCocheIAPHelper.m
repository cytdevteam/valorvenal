//
//  InAppTasaCocheIAPHelper.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 15/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InAppTasaCocheIAPHelper.h"

#define kProVersionProductIdentifier @"QWG86342EN.com.cytdevteam.tasacoche.20120202001IAP001"

@implementation InAppTasaCocheIAPHelper

static InAppTasaCocheIAPHelper * _sharedHelper;

+ (InAppTasaCocheIAPHelper *)sharedHelper
{
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[InAppTasaCocheIAPHelper alloc] init];
    return _sharedHelper;
}

- (id)init {
    
    NSSet *productIdentifiers = [NSSet setWithObjects:
                                 kProVersionProductIdentifier,
                                 nil];
    
    if ((self = [super initWithProductIdentifiers:productIdentifiers])) {                
        
    }
    return self;
    
}

+ (BOOL)isProVersion
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:kProVersionProductIdentifier]) {
        return (BOOL)[userDefaults objectForKey:kProVersionProductIdentifier];
    }
    return NO;
}



+ (void)buyProVersion
{
    
}


@end
