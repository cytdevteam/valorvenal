//
//  JMObjectSelectionViewController.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMObjectSelectionViewController.h"
#import "CoreDataHelper.h"
#import "JMCellStyleSubtitle.h"
#import "TasacionViewController.h"


@implementation JMObjectSelectionViewController

@synthesize delegate;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObject = _managedObject;

@synthesize entityName = _entityName;
@synthesize titleProperty = _titleProperty;
@synthesize subtitleProperty = _subtitleProperty;
@synthesize orderArray = _orderArray;
@synthesize entityArray = _entityArray;
@synthesize entitySearchPredicate = _entitySearchPredicate;
@synthesize titleNavBar = _titleNavBar;
@synthesize groupBy = _groupBy;
@synthesize orderKeyForGroupBy = _orderKeyForGroupBy;

@synthesize objectSelected = _objectSelected;
@synthesize indexPath = _indexPath;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor underPageBackgroundColor];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = self.titleNavBar; 
    
    if (self.entityArray == nil) {
        [self populateArrayFromRequest];
    }
    
    [delegate isAboutToShowTheList];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.entityArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell
	NSManagedObject *otherManagedObject = [self.entityArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [JMCellStyleSubtitle cellForTableView:tableView];
    if ([self.managedObject valueForKey:self.objectSelected] == otherManagedObject) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    if ([[self.entityArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
        cell.textLabel.text = [self.entityArray objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [otherManagedObject valueForKey:self.titleProperty];
        if (self.subtitleProperty) {
            cell.detailTextLabel.text = [otherManagedObject valueForKey:self.subtitleProperty];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSManagedObject *otherManagedObject = [self.entityArray objectAtIndex:indexPath.row];
    NSString *title = nil;
    NSString *subtitle = nil;
    if ([[self.entityArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
        title = [self.entityArray objectAtIndex:indexPath.row];
    } else {
        title = [otherManagedObject valueForKey:self.titleProperty];
        if (self.subtitleProperty) {
            subtitle = [otherManagedObject valueForKey:self.subtitleProperty];
        }
    }
    
    return [JMCellStyleSubtitle heightForRowWithTitle:title andSubtitle:subtitle];
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // If there was a previous selection, unset the accessory view for its cell.
	NSManagedObject *currentType = [self.managedObject valueForKey:self.objectSelected];
    
    if (currentType != nil) {
		NSInteger index = [self.entityArray indexOfObject:currentType];
		NSIndexPath *selectionIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        UITableViewCell *checkedCell = [tableView cellForRowAtIndexPath:selectionIndexPath];
        checkedCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    // Set the checkmark accessory for the selected row.
    [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];    
    
    // Update the type of the recipe instance
    [self.managedObject setValue:[self.entityArray objectAtIndex:indexPath.row] forKey:self.objectSelected];
    
    // Deselect the row.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (currentType != nil && currentType != [self.entityArray objectAtIndex:indexPath.row] && _indexPath.section == 0) {
        [delegate selectionHasChangedForObjectSelected:_indexPath]; 
    } else if (_indexPath.section == 1) {
        [self tasaVehiculoSeleccionado];     
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    // Go back.
}

#pragma mark - My own methods


-(void)populateArrayFromRequest {
    if (self.entitySearchPredicate == nil) {
        
        NSMutableArray *mutableFetchResults = nil;
        if (self.orderArray) {
            mutableFetchResults = [CoreDataHelper searchObjectsInContext:self.entityName :nil :self.orderArray :self.managedObjectContext];
        } else {
            mutableFetchResults = [CoreDataHelper getObjectsFromContext:self.entityName :self.titleProperty :YES :self.managedObjectContext];
        }
        if (self.groupBy) {
            NSMutableArray *distinctArray = [mutableFetchResults valueForKeyPath:self.groupBy];
            if ([distinctArray count] > 0 && [[distinctArray objectAtIndex:0] isKindOfClass:[NSString class]]) {
                self.entityArray = [[mutableFetchResults valueForKeyPath:self.groupBy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            } else {
                NSSortDescriptor *sortDescriptor;
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:self.orderKeyForGroupBy
                                                             ascending:YES];
                NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                self.entityArray = [distinctArray sortedArrayUsingDescriptors:sortDescriptors];
            }
        } else {
            self.entityArray = mutableFetchResults;

        }
    } else {
        
        NSMutableArray *mutableFetchResults = nil;
        if (self.orderArray) {
            mutableFetchResults = [CoreDataHelper searchObjectsInContext:self.entityName :self.entitySearchPredicate :self.orderArray :self.managedObjectContext];
        } else {
            mutableFetchResults = [CoreDataHelper searchObjectsInContext:self.entityName :self.entitySearchPredicate :self.titleProperty :YES :self.managedObjectContext];

        }
        if (self.groupBy) {
            NSMutableArray *distinctArray = [mutableFetchResults valueForKeyPath:self.groupBy];
            if ([distinctArray count] > 0 && [[distinctArray objectAtIndex:0] isKindOfClass:[NSString class]]) {
                self.entityArray = [[mutableFetchResults valueForKeyPath:self.groupBy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            } else {
                NSSortDescriptor *sortDescriptor;
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:self.orderKeyForGroupBy
                                                              ascending:YES];
                NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                self.entityArray = [distinctArray sortedArrayUsingDescriptors:sortDescriptors];
            }
        } else {
            self.entityArray = mutableFetchResults;
            
        }
    }    
    
}

- (BOOL)requestWithEmptyArrayForObjectSelected
{
    
    if (self.entityArray == nil) {
        [self populateArrayFromRequest];
    }

    if ([self.entityArray count] == 0) {
        return YES;
    }
    return NO;
    
}

- (void)tasaVehiculoSeleccionado
{
    TasacionViewController *viewController = [[TasacionViewController alloc] initWithStyle:UITableViewStyleGrouped];
    viewController.tasacion = self.managedObject;
    viewController.allowSaving = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


@end
