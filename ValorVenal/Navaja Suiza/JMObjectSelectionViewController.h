//
//  JMObjectSelectionViewController.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JMObjectSelectionDelegate;

@interface JMObjectSelectionViewController : UITableViewController {}

@property (nonatomic, unsafe_unretained) id <JMObjectSelectionDelegate> delegate; 

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObject *managedObject;

@property (nonatomic, strong) NSString *entityName;
@property (nonatomic, strong) NSString *titleProperty;
@property (nonatomic, strong) NSString *subtitleProperty;
@property (nonatomic, strong) NSMutableArray *orderArray;
@property (nonatomic, strong) NSPredicate *entitySearchPredicate;
@property (nonatomic, strong) NSString *titleNavBar;
@property (nonatomic, strong) NSString *groupBy;
@property (nonatomic, strong) NSString *orderKeyForGroupBy;

@property (nonatomic, strong) NSString *objectSelected;
@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, strong) NSArray *entityArray;

- (void)populateArrayFromRequest;
- (BOOL)requestWithEmptyArrayForObjectSelected;
- (void)tasaVehiculoSeleccionado;


@end

@protocol JMObjectSelectionDelegate

- (void)isAboutToShowTheList;
- (BOOL)requestWithEmptyArrayForObjectSelected:(NSString *)objectSelected;
- (void)selectionHasChangedForObjectSelected:(NSIndexPath *)indexPath;

@end
