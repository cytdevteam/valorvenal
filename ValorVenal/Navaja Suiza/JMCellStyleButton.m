//
//  JMCellStyleButton.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMCellStyleButton.h"

@implementation JMCellStyleButton

@synthesize title;
@synthesize color;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithCellIdentifier:(NSString *)cellID 
{
    if ((self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID])) {
        self.textLabel.textAlignment = UITextAlignmentCenter;
    } else {
        if ([self.contentView subviews]){
            for (UIView *subview in [self.contentView subviews]) {
                [subview removeFromSuperview];
            }
        }
    }
    
    //creates the button look
    int pos=0;
    int width=300;
//    if (!portraitOrientation)
//    {
//        pos=55;
//        width=345;
//    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(pos, 0, width, 43)];
    [view setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setCornerRadius:10.0f];
    [gradient setMasksToBounds:YES];
    [gradient setBorderWidth:1.6f];
    [gradient setBorderColor:[[UIColor darkGrayColor] CGColor]];
//    [gradient setBackgroundColor:[[UIColor blueColor] CGColor]];
    [gradient setBackgroundColor:[[UIColor colorWithRed:0.0/255.0 green:174.0/255.0 blue:255.0/255.0 alpha:1] CGColor]];
    gradient.frame = view.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                       (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                       (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                       (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                       (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                       nil];
    gradient.locations = [NSArray arrayWithObjects:
                          [NSNumber numberWithFloat:0.0f],
                          [NSNumber numberWithFloat:0.5f],
                          [NSNumber numberWithFloat:0.5f],
                          [NSNumber numberWithFloat:0.8f],
                          [NSNumber numberWithFloat:1.0f],
                          nil];
    
    [view.layer insertSublayer:gradient atIndex:0];
    [self.contentView addSubview:view];
    
    //"button" label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(pos, 3, width, 37)];
    [label setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont boldSystemFontOfSize:23.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
    label.backgroundColor = [UIColor clearColor];
    label.text = @"Buscar vehiculo";
    [self.contentView addSubview: label];
    
    //makes cell transparent
    UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
    [backView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    backView.backgroundColor = [UIColor clearColor];
    self.backgroundView = backView;
    self.backgroundColor = [UIColor clearColor];
    
    return self;
    
}

@end
