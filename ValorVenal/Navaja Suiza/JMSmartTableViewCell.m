//
//  JMSmartTableViewCell.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 02/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMSmartTableViewCell.h"

@implementation JMSmartTableViewCell

// Esto es lo que venía de serie.

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// y esto lo que @drance nos sujiere que hagamos:

+ (id)cellForTableView:(UITableView *)tableView
{
    NSString *cellID = [self cellIdentifier];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[self alloc] initWithCellIdentifier:cellID];
    }
    return cell;
}

+ (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

- (id)initWithCellIdentifier:(NSString *)cellID
{
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
}

@end
