//
//  InAppTasaCocheIAPHelper.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 15/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAPHelper.h"

@interface InAppTasaCocheIAPHelper : IAPHelper

+ (InAppTasaCocheIAPHelper *)sharedHelper;

+ (BOOL)isProVersion;

@end
