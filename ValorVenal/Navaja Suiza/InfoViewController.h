//
//  InfoViewController.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 21/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "MBProgressHUD.h"


@interface InfoViewController : UITableViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) MBProgressHUD *hud;

- (void)volverVistaPrincipal;
- (IBAction)actionEmailComposer;
- (IBAction)buyButtonTapped:(id)sender;
- (IBAction) showAppInfo:(id)sender;

@end
