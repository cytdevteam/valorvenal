//
//  FAQViewController.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 25/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FAQViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *arrayFAQ;

- (int) heightOfCellWithTitle:(NSString*)titleText andSubtitle:(NSString*)subtitleText;

@end
