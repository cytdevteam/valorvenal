//
//  JMCellStyleSubtitle.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 11/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMCellStyleSubtitle.h"

@implementation JMCellStyleSubtitle

#define CONST_Cell_height 44.0f
#define CONST_Cell_width 270.0f

#define CONST_textLabelFontSize 17
#define CONST_detailLabelFontSize 15

- (id)initWithCellIdentifier:(NSString *)cellID 
{
    if ((self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID])) {
        self.textLabel.numberOfLines = 0;
        self.textLabel.font = [UIFont boldSystemFontOfSize:CONST_textLabelFontSize];
        self.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        self.detailTextLabel.numberOfLines = 0;
        self.detailTextLabel.font = [UIFont boldSystemFontOfSize:CONST_detailLabelFontSize];
        self.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
    }
    
    return self;
    
}

+ (int) heightOfCellWithTitle:(NSString*)titleText andSubtitle:(NSString*)subtitleText
{
    CGSize titleSize = {0, 0}; CGSize subtitleSize = {0, 0};
    
    if (titleText && ![titleText isEqualToString:@""])
        titleSize = [titleText sizeWithFont:[UIFont boldSystemFontOfSize:CONST_textLabelFontSize] constrainedToSize:CGSizeMake(CONST_Cell_width, 4000) lineBreakMode:UILineBreakModeWordWrap];
    
    if (subtitleText && ![subtitleText isEqualToString:@""])   
        subtitleSize = [subtitleText sizeWithFont:[UIFont systemFontOfSize:CONST_detailLabelFontSize]  constrainedToSize:CGSizeMake (CONST_Cell_width, 4000) lineBreakMode:UILineBreakModeWordWrap];
    
    return titleSize.height + subtitleSize.height;
}

+ (CGFloat)heightForRowWithTitle:(NSString *)titleText andSubtitle:(NSString *)subtileText
{
    int height = 10 + [self heightOfCellWithTitle:titleText andSubtitle:subtileText];
    return (height < CONST_Cell_height ? CONST_Cell_height : height);
}


@end
