//
//  JMCellStyleSubtitle.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 11/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMSmartTableViewCell.h"

@interface JMCellStyleSubtitle : JMSmartTableViewCell {}

+ (int) heightOfCellWithTitle:(NSString*)titleText andSubtitle:(NSString*)subtitleText;
+ (CGFloat)heightForRowWithTitle:(NSString *)titleText andSubtitle:(NSString *)subtileText;


@end
