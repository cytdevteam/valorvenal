//
//  InfoViewController.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 21/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InfoViewController.h"

#import "JMCellStyleValue1.h"
#import "FAQViewController.h"

#import "InAppTasaCocheIAPHelper.h"
#import "Reachability.h"

// START:TableSections
enum JMTableSections {
    JMTableSectionApplication = 0,
    JMTableSectionEnterprise,
    JMTableNumSections,
};
// END:TableSections

// START:TableRows
enum JMApplicationRows {
    JMTSApplicationRowFAQ = 0,
    JMTSApplicationRowIAP,    
    JMTSApplicationRowValoration,    
    JMTSApplicationNumRows,
};

// START:TableRows
enum JMEnterpriseRows {
    JMTSEnterpriseRowContact = 0,
    JMTSEnterpriseRowAbout,
    JMTSEnterpriseRowOtherApps,
    JMTSEnterpriseNumRows,
};

BOOL proVersion;

@implementation InfoViewController

@synthesize hud = _hud;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(volverVistaPrincipal)];
    self.navigationItem.leftBarButtonItem = doneButtonItem;

    self.navigationController.title = NSLocalizedString(@"Información", @"Información");
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.tableView.backgroundColor = [UIColor underPageBackgroundColor];

}

- (void)volverVistaPrincipal
{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.hud = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsLoaded:) name:kProductsLoadedNotification object:nil];
    
    proVersion = [InAppTasaCocheIAPHelper isProVersion];
    
    if (!proVersion) {
        Reachability *reach = [Reachability reachabilityForInternetConnection];	
        NetworkStatus netStatus = [reach currentReachabilityStatus];    
        if (netStatus == NotReachable) {        
            NSLog(@"No internet connection!");        
        } else {        
            if ([InAppTasaCocheIAPHelper sharedHelper].products == nil) {
                
                [[InAppTasaCocheIAPHelper sharedHelper] requestProducts];
                self.hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                _hud.labelText = NSLocalizedString(@"Loading price...", @"Loading price...");
                [self performSelector:@selector(timeout:) withObject:nil afterDelay:30.0];
                
            }        
        }
    }
    
    // Add inside viewWillAppear
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object: nil];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return JMTableNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // START:NumRows
    switch (section) {
        case JMTableSectionApplication:
            return JMTSApplicationNumRows;
        case JMTableSectionEnterprise:
            return JMTSEnterpriseNumRows;
        default:
            NSLog(@"Unexpected section (%d)", section);
            break;
    }
    // END:NumRows
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    // START:SearchingSection
    switch (indexPath.section) {
        case JMTableSectionApplication: {
            cell = [JMCellStyleValue1 cellForTableView:tableView];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            switch (indexPath.row) {
                case JMTSApplicationRowFAQ:
                    cell.textLabel.text = NSLocalizedString(@"Preguntas frecuentes", @"Preguntas frecuentes");
                    break;
                case JMTSApplicationRowIAP:{
                    if (proVersion) {
                        cell.textLabel.text = NSLocalizedString(@"Valor Venal Pro", @"Valor Venal Pro");
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        cell.accessoryView = nil;
                    } else {
                        SKProduct *product = [[InAppTasaCocheIAPHelper sharedHelper].products objectAtIndex:0];
                        
                        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
                        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                        [numberFormatter setLocale:product.priceLocale];
                        NSString *formattedString = [numberFormatter stringFromNumber:product.price];
                        
                        cell.textLabel.text = NSLocalizedString(@"Valor Venal Pro", @"Valor Venal Pro");
                        cell.detailTextLabel.text = formattedString;                        
                    }
                }
                    break;
                case JMTSApplicationRowValoration:
                    cell.textLabel.text = NSLocalizedString(@"Puntuanos", @"Puntuanos");
                    break;
                default:
                    NSAssert1(NO, @"Unexpected row in Favorites section: %d",
                              indexPath.row);
                    break;
            }
            break;
        }
        case JMTableSectionEnterprise:
            cell = [JMCellStyleValue1 cellForTableView:tableView];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            switch (indexPath.row) {
                case JMTSEnterpriseRowContact:
                    // Create and configure the disclosure button.
                    cell.textLabel.text = NSLocalizedString(@"Contacto", @"Contacto");
                    break;
                case JMTSEnterpriseRowAbout:
                    // Create and configure the disclosure button.
                    cell.textLabel.text = NSLocalizedString(@"Acerca de...", @"Acerca de...");
                    break;
                case JMTSEnterpriseRowOtherApps:
                    // Create and configure the disclosure button.
                    cell.textLabel.text = NSLocalizedString(@"Nuestras app´s", @"Nuestras app´s");
                    break;
                default:
                    NSAssert1(NO, @"Unexpected row in Favorites section: %d",
                              indexPath.row);
                    break;
            }
            break;
        default:
            NSAssert1(NO, @"Unexpected section (%d)", indexPath.section);
            break;
            // END:SubmitSection
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // START:SearchingSection
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case JMTableSectionApplication: {
            switch (indexPath.row) {
                case JMTSApplicationRowFAQ: {
                    // aqui ira la conexión con las preguntas frecuentes
                    FAQViewController *viewController = [[FAQViewController alloc] initWithStyle:UITableViewStylePlain];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
                    break;                    
                case JMTSApplicationRowIAP: 
                    // aquí ira para comprar el resto de la aplicación
                    [self buyButtonTapped:nil];
                    break;
                case JMTSApplicationRowValoration:
                    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.com/apps/tasacoche"]];                        
                    break;
                default:
                    NSAssert1(NO, @"Unexpected row in Favorites section: %d",
                              indexPath.row);
                    break;
            }
            break;
        }
        case JMTableSectionEnterprise:
            switch (indexPath.row) {
                case JMTSEnterpriseRowContact:
                    [self actionEmailComposer];
                    break;
                case JMTSEnterpriseRowAbout:
                    // Aquí va algo de información de la empresa...
                    [self showAppInfo:nil];
                    break;
                case JMTSEnterpriseRowOtherApps:
                    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.com/apps/cytdevteam"]];                      
                    break;
                default:
                    NSAssert1(NO, @"Unexpected row in Favorites section: %d",
                              indexPath.row);
                    break;
            }
            break;
        default:
            NSAssert1(NO, @"Unexpected section (%d)", indexPath.section);
            break;
            // END:SubmitSection
    }
}

- (IBAction)actionEmailComposer {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        mailViewController.navigationBar.barStyle = UIBarStyleBlack;
        [mailViewController setToRecipients:[NSArray arrayWithObject:@"soporte.cdt@gmail.com"]];
        [mailViewController setSubject:@"Contacto desde ValorVenal"];
//        [mailViewController setMessageBody:@"Your message goes here." isHTML:NO];
        
        [self presentModalViewController:mailViewController animated:YES];
        
    }
    
    else {
        
        NSLog(@"Device is unable to send email in its current state.");
        
    }
    
}

#pragma mark Actions
- (IBAction) showAppInfo:(id)sender
{
	// a convenient method to get to the Info.plist in the app bundle
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
	
	// get two items from the dictionary
	NSString *version = [info objectForKey:@"CFBundleVersion"];
	NSString *title = [info objectForKey:@"CFBundleDisplayName"];
	
	UIAlertView *alert = [[UIAlertView alloc] 
						  initWithTitle:[@"About " stringByAppendingString:title]
						  message:[NSString stringWithFormat:@"Version %@\n\n© 2012 CYT Dev Team\nAll rights reserved.", version]
						  delegate:self 
						  cancelButtonTitle:@"OK" 
						  otherButtonTitles:nil];
	[alert show];
}


-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    [self dismissModalViewControllerAnimated:YES];
    
}


- (void)dismissHUD:(id)arg {
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    self.hud = nil;
    
}

- (void)productsLoaded:(NSNotification *)notification {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    self.tableView.hidden = FALSE;    
    
    [self.tableView reloadData];
    
}

- (void)timeout:(id)arg {
    
    _hud.labelText = @"Timeout!";
    _hud.detailsLabelText = @"Please try again later.";
    _hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.jpg"]];
	_hud.mode = MBProgressHUDModeCustomView;
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
    
}

// Add new method
- (IBAction)buyButtonTapped:(id)sender {
    
    SKProduct *product = [[InAppTasaCocheIAPHelper sharedHelper].products objectAtIndex:0];
    
    NSLog(@"Buying %@...", product.productIdentifier);
    if (product) {
        [[InAppTasaCocheIAPHelper sharedHelper] buyProductIdentifier:product.productIdentifier];
        
        self.hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        _hud.labelText = @"Buying fable...";
        [self performSelector:@selector(timeout:) withObject:nil afterDelay:60*5];
    }
    
}

// Add new methods
- (void)productPurchased:(NSNotification *)notification {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];    
    
    NSString *productIdentifier = (NSString *) notification.object;
    NSLog(@"Purchased: %@", productIdentifier);
    
    proVersion = YES;
    
    [self.tableView reloadData];    
    
}

- (void)productPurchaseFailed:(NSNotification *)notification {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
    SKPaymentTransaction * transaction = (SKPaymentTransaction *) notification.object;    
    if (transaction.error.code != SKErrorPaymentCancelled) {    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" 
                                                         message:transaction.error.localizedDescription 
                                                        delegate:nil 
                                               cancelButtonTitle:nil 
                                               otherButtonTitles:@"OK", nil];
        
        [alert show];
    }
    
}


@end
