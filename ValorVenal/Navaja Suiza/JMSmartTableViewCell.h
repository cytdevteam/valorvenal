//
//  JMSmartTableViewCell.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 02/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMSmartTableViewCell : UITableViewCell {}

+ (id)cellForTableView:(UITableView *)tableView;
+ (NSString *)cellIdentifier;

- (id)initWithCellIdentifier:(NSString *)cellID;

@end
