//
//  JMCellStyleDefault.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 02/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMCellStyleDefault.h"

@implementation JMCellStyleDefault

- (id)initWithCellIdentifier:(NSString *)cellID 
{
    if ((self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID])) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.indentationLevel = 0;
        
    }
    
    return self;
    
}

@end
