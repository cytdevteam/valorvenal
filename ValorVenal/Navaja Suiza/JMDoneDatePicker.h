//
//  JMDoneDatePicker.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 28/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MyDateTimePickerHeight 260

@interface JMDoneDatePicker : UIView

@property (nonatomic, assign, readonly) UIDatePicker *picker;

- (void) setMode: (UIDatePickerMode) mode;
- (void) setHidden: (BOOL) hidden animated: (BOOL) animated;
- (void) addTargetForDoneButton: (id) target action: (SEL) action;

@end

#define MyDateTimePickerPickerHeight 216
#define MyDateTimePickerToolbarHeight 44
#define MyConstantsElementAnimationLength 1

@interface JMDoneDatePicker() 

@property (nonatomic, assign, readwrite) UIDatePicker *picker;
@property (nonatomic, assign) CGRect originalFrame;

@property (nonatomic, assign) id doneTarget;
@property (nonatomic, assign) SEL doneSelector;

- (void) donePressed;

@end



