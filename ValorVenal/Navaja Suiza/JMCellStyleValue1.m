//
//  JMCellStyleValue1.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 02/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMCellStyleValue1.h"

@implementation JMCellStyleValue1

- (id)initWithCellIdentifier:(NSString *)cellID 
{
    if ((self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID])) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.indentationLevel = 0;

    }
    
    return self;
    
}

@end
