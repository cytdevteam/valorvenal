//
//  JMCellInputDate.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 09/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMCellInputDate.h"

@implementation JMCellInputDate

- (id)initWithCellIdentifier:(NSString *)cellID 
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    if ((self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID])) {
        self.detailTextLabel.numberOfLines = 0;
        self.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
        self.textLabel.numberOfLines = 0;
        self.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    }
    
	self.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate date]];
	
    return self;
    
}


@end
