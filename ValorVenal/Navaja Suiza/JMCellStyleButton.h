//
//  JMCellStyleButton.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMSmartTableViewCell.h"

@interface JMCellStyleButton : JMSmartTableViewCell

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIColor *color;

@end
