//
//  JMDoneDatePicker.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 28/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JMDoneDatePicker.h"

@implementation JMDoneDatePicker

@synthesize picker = _picker;
@synthesize originalFrame = _originalFrame;

@synthesize doneTarget = _doneTarget;
@synthesize doneSelector = _doneSelector;

- (id) initWithFrame: (CGRect) frame {
    if ((self = [super initWithFrame: frame])) {
        self.originalFrame = frame;
        self.backgroundColor = [UIColor clearColor];
        
        CGFloat width = self.bounds.size.width;
        UIDatePicker *picker = [[[UIDatePicker alloc] initWithFrame: CGRectMake(0, 0, width, MyDateTimePickerPickerHeight)] autorelease];
        [self addSubview: picker];
        
        UIToolbar *toolbar = [[[UIToolbar alloc] initWithFrame: CGRectMake(0, MyDateTimePickerPickerHeight, width, MyDateTimePickerToolbarHeight)] autorelease];
        toolbar.barStyle = UIBarStyleBlackOpaque;
        
        UIBarButtonItem *doneButton = [[[UIBarButtonItem alloc] initWithTitle: @"Done" style: UIBarButtonItemStyleBordered target: self action: @selector(donePressed)] autorelease];
        doneButton.width = width - 20;
        toolbar.items = [NSArray arrayWithObject: doneButton];
        [self addSubview: toolbar];
        
        self.picker = picker;
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (void) setMode: (UIDatePickerMode) mode {
    self.picker.datePickerMode = mode;
}

- (void) donePressed {
    if (self.doneTarget) {
        [self.doneTarget performSelector: self.doneSelector];
    }
}

- (void) addTargetForDoneButton: (id) target action: (SEL) action {
    self.doneTarget = target;
    self.doneSelector = action;
}

- (void) setHidden: (BOOL) hidden animated: (BOOL) animated {
    CGRect newFrame = self.originalFrame;
    newFrame.origin.y += hidden ? MyDateTimePickerHeight : 0;
    if (animated) {
        [UIView beginAnimations: @"animateDateTimePicker" context: nil];
        [UIView setAnimationDuration: MyConstantsElementAnimationLength];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
        
        self.frame = newFrame;      
        
        [UIView commitAnimations]; 
    } else {
        self.frame = newFrame;      
    }
}

@end