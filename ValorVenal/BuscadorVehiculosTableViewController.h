//
//  BuscadorVehiculosTableViewController.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 02/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tasaciones.h"
#import "JMObjectSelectionViewController.h"
#import "JMDoneDatePicker.h"
#import "MBProgressHUD.h"

#import <iAd/iAd.h>
#import "GADBannerView.h"

@class JMObjectSelectionViewController;


@interface BuscadorVehiculosTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIAlertViewDelegate, JMObjectSelectionDelegate, MBProgressHUDDelegate, ADBannerViewDelegate, GADBannerViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) JMObjectSelectionViewController *selectionViewController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) Tasaciones *tasacion;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) MBProgressHUD *HUD;

@property (nonatomic, strong) ADBannerView *bannerView;
@property (nonatomic, strong) GADBannerView *admobBannerView;

- (void)showInfoViewController;
- (void)showActionSheet;
- (BOOL)fechaDeMatriculacionEsCorrecta;
- (NSPredicate *)predicateForModelos;
- (NSPredicate *)predicateForModelComprobation;
- (NSPredicate *)predicateForAcabadosWithRow:(NSIndexPath *)indexPath;
- (NSString *)informaAntesDeLaFila:(NSInteger)row;
- (NSString *)validaObligatorios;
- (BOOL)avisoDeLimpiezaPorCambioEnFila:(NSInteger)row;
- (void)nuevaTasacion;
- (UIViewController *)viewControllerForSearchingSelection:(NSIndexPath *)indexPath;
- (UIViewController *)viewControllerForSubmittingSelection:(NSIndexPath *)indexPath;
- (void)requestWithEmptyArrayForObjectSelected:(NSString *)objectSelected;

- (void)DatePickerView;

- (IBAction)showSimple:(id)sender;
- (IBAction)hideSimple:(id)sender;

@end
