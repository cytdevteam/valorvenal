//
//  JMFactoresMinoracion.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 31/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JMFactoresMinoracion : NSObject

+(NSNumber *)valorVenalParaAntiguedad:(NSInteger)years conValorDeReferencia:(NSNumber *)valorReferencia;

+(NSMutableArray *)valoresVenalesSucesivosParaValorDeReferencia:(NSNumber *)valorReferencia;

@end
