//
//  TasacionViewController.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 11/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tasaciones.h"

@interface TasacionViewController : UITableViewController 

@property (nonatomic, strong) NSArray *arrayPorcentajes;
@property (nonatomic, strong) Tasaciones *tasacion;
@property (nonatomic, strong) UIButton *disclosureButton;
@property BOOL collapsed;
@property BOOL allowSaving;
@property BOOL isProVersion;

- (void)openSection;
- (void)closeSection;
- (void)save;

@end
