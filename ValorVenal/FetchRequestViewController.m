//
//  CalendariosViewController.m
//  UtilBB
//
//  Created by Javier Moreno Lozano on 26/10/10.
//  Copyright (c) 2010 __MyCompanyName__. All rights reserved.
//

#import "FetchRequestViewController.h"
#import "AcabadoViewController.h"

#import "Marcas.h"
#import "Modelos.h"
#import "Acabados.h"

#define KTitleValueTag 1
#define KSubtitle1ValueTag 2
#define KSubtitle2ValueTag 3

#define KOrigenX 20
#define KOrigenY 10
#define KAncho 250


@implementation FetchRequestViewController

@synthesize managedObjectContext;

@synthesize entityName;
@synthesize orderKey;
@synthesize orderArray;
@synthesize entityArray;
@synthesize entitySearchPredicate;
@synthesize titleNavBar;
@synthesize HUD;


#pragma mark -
#pragma mark Initialization

/*
 - (id)initWithStyle:(UITableViewStyle)style {
 // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 if ((self = [super initWithStyle:style])) {
 }
 return self;
 }
 */


#pragma mark -
#pragma mark View lifecycle


-(void)cargaDatos {
    NSMutableArray *mutableFetchResults = nil;
    if (entitySearchPredicate == nil) {
        if (orderArray == nil) {
            mutableFetchResults = [CoreDataHelper getObjectsFromContext:entityName :orderKey :YES :managedObjectContext];
        } else {
            mutableFetchResults = [CoreDataHelper searchObjectsInContext:entityName :entitySearchPredicate :orderArray :managedObjectContext];
        }
        self.entityArray = mutableFetchResults;
    } else {
        if (orderArray == nil) {
            mutableFetchResults = [CoreDataHelper searchObjectsInContext:entityName :entitySearchPredicate :orderKey :YES :managedObjectContext];
        } else {
            mutableFetchResults = [CoreDataHelper searchObjectsInContext:entityName :entitySearchPredicate :orderArray :managedObjectContext];
        }
        self.entityArray = mutableFetchResults;
    }
}
/*
 -(NSMutableArray *)searchObjectsInContext:(NSString *)entity predicate:(NSPredicate *)predicate sortKey:(NSString *)sortKey ascending:(BOOL)ascending managedObjectContext:(NSManagedObjectContext *)managedObjectContext {
 
 NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
 NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
 [fetchRequest setEntity:entityDescription];
 
 if (predicate != nil) {
 [fetchRequest setPredicate:predicate];
 }
 
 if (sortKey != nil) {
 NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending];
 NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
 [fetchRequest setSortDescriptors:sortDescriptors];
 [sortDescriptor release];
 [sortDescriptors release];
 }
 
 NSError *error;
 NSMutableArray *mutableFetchResults = [[[managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy] autorelease];
 [fetchRequest release];
 
 return mutableFetchResults;
 
 }
 
 
 -(NSMutableArray *)getObjectsFromContext:(NSString *)entity sortKey:(NSString *)sortKey ascending:(BOOL)ascending managedObjectContext:(NSManagedObjectContext *) managedObjectContext {
 return [self searchObjectsInContext:entityName predicate:nil sortKey:sortKey ascending:YES managedObjectContext:managedObjectContext];
 }
 */

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.title = self.titleNavBar;
    if (self.tableView.style == UITableViewStyleGrouped) {
        self.tableView.backgroundColor = [UIColor underPageBackgroundColor];
    }
    
    if (self.entityArray == nil) {
        [self cargaDatos];
    }
    
}


- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    //    [super viewWillAppear:animated];
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [entityArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    NSManagedObject *object = (NSManagedObject *)[entityArray objectAtIndex:indexPath.row];
    
    NSString *title = nil;
    NSString *subtitle = nil;
    int cellStyle = UITableViewCellStyleDefault;
    
    if (entityName == @"Marcas") {
        cellStyle = UITableViewCellStyleDefault;
        title = [object valueForKey:@"marca"];
    } else if (entityName == @"Modelos") {
        cellStyle = UITableViewCellStyleDefault;
        title = [object valueForKey:@"modelo"];
    } else if (entityName == @"Acabados") {
        cellStyle = UITableViewCellStyleSubtitle;
        title = [object valueForKey:@"acabado"];
        subtitle = [object valueForKey:@"periodoComercial"];
    }
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:cellStyle reuseIdentifier:CellIdentifier] autorelease];        
    }
    
    cell.textLabel.text = title;
    cell.detailTextLabel.text = subtitle;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Recuperación de datos.


#pragma mark -
#pragma mark Table view delegate

- (void)preparaLaNuevaVistaParaElObjeto:(NSManagedObject *)selectedObject 
{
    if ((self.entityName == @"Marcas") || (self.entityName == @"Modelos")) {
        FetchRequestViewController *viewController = [[FetchRequestViewController alloc] initWithStyle:UITableViewStyleGrouped];
        viewController.managedObjectContext = self.managedObjectContext;
        NSPredicate *predicate = nil;
        
        if (self.entityName == @"Marcas") {
            viewController.entityName = @"Modelos";
            viewController.orderKey = @"modelo";
            predicate = [NSPredicate predicateWithFormat:@"(marca == %@)", selectedObject];
            viewController.titleNavBar = [selectedObject valueForKey:@"marca"];
        } else if (self.entityName == @"Modelos") {
            viewController.entityName = @"Acabados";
            viewController.orderArray = [NSMutableArray arrayWithObjects:@"periodoComercial", @"acabado", nil];
            predicate = [NSPredicate predicateWithFormat:@"(modelos CONTAINS %@)", selectedObject];        
            viewController.titleNavBar = [selectedObject valueForKey:@"modelo"];
        } 
        
        [viewController setEntitySearchPredicate:predicate];
        
        [self.navigationController pushViewController:viewController animated:YES];
        [viewController release];
        
    } else if (self.entityName == @"Acabados") {
        AcabadoViewController *viewController = [[AcabadoViewController alloc] initWithStyle:UITableViewStyleGrouped];
        viewController.acabado = selectedObject;
        [self.navigationController pushViewController:viewController animated:YES];
        [viewController release];        
    }
//    [self hideSimple:nil];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    [self showSimple:nil];
    NSManagedObject *selectedObject = [entityArray objectAtIndex:indexPath.row];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;

    [HUD showWhileExecuting:@selector(preparaLaNuevaVistaParaElObjeto:) onTarget:self withObject:selectedObject animated:YES];    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    if (entitySearchPredicate != nil) {
        [entitySearchPredicate release];
    }
    [orderKey release];
    [orderArray release];
	[entityArray release];
	[entityName release];
	[managedObjectContext release];
    [super dealloc];
}

#pragma mark -
#pragma mark IBActions

- (IBAction)showSimple:(id)sender {
    // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
	
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
	
    // Show the HUD while the provided method executes in a new thread
    [HUD show:YES];
    //    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}

- (IBAction)hideSimple:(id)sender
{
    [HUD hide:YES];
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    //    [HUD release];
	HUD = nil;
}

@end
