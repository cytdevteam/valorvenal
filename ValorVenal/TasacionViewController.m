//
//  TasacionViewController.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 11/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TasacionViewController.h"
#import "JMCellStyleValue1.h"
#import "JMFactoresMinoracion.h"

#import "InAppTasaCocheIAPHelper.h"

// START:TableSections
enum JMTableSections {
    JMTableSectionTasacion = 0,
    JMTableSectionFichaTecnica,
    JMTableNumSections,
};
// END:TableSections

// START:TableRows
enum JMTasacionRows {
//    JMTableSecTasacionRowMarca = 0,
//    JMTableSecTasacionRowAcabado,
    JMTableSecTasacionRowFechaMatriculacion = 0,
    JMTableSecTasacionRowAntiguedad,
    JMTableSecTasacionRowValorActual,
    JMTableSecTasacionNumRows,
};

// START:TableRows
enum JMFichaTecnicaElapsedRows {
    JMTableSecFichaRowPeriodoComercial = 0,
    JMTableSecFichaRowValorNuevo,
    JMTableSecFichaRowCaballosVapor,
    JMTableSecFichaRowTitulo,
    JMTableSecFichaRowCilindrada,
    JMTableSecFichaRowCilindros,
    JMTableSecFichaRowTipoCombustible,
    JMTableSecFichaRowPotenciaKWh,
    JMTableSecFichaRowCaballosFiscales,
    JMTableSecFichaRowEmisionCO2,
    JMTableSecFichaNumRows,
};
// START:TableRows
enum JMFichaTecnicaCollapsedRows {
    JMTableSecCollapsedFichaRowPeriodoComercial = 0,
    JMTableSecCollapsedFichaRowValorNuevo,
    JMTableSecCollapsedFichaRowCaballosVapor,
    JMTableSecCollapsedFichaRowTitulo,
    JMTableSecCollapsedFichaNumRows,
};




@implementation TasacionViewController

@synthesize arrayPorcentajes;
@synthesize collapsed;
@synthesize tasacion;
@synthesize disclosureButton;
@synthesize allowSaving;
@synthesize isProVersion;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (allowSaving) {
        UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save)];
        self.navigationItem.rightBarButtonItem = saveButtonItem;
    }
    
    
    isProVersion = [InAppTasaCocheIAPHelper isProVersion];
    //    NSLog(@"isProVersion: %@", isProVersion);
    
    //a partir de aquí vienen los metodos de iAd:
    if (!isProVersion) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    self.title = NSLocalizedString(@"Tasación", @"Tasación");
    self.tableView.backgroundColor = [UIColor underPageBackgroundColor];
        
    int uso = [tasacion.antiguedad intValue];
    NSManagedObject *acabado = (NSManagedObject *)tasacion.acabado;
    
    tasacion.precio = [JMFactoresMinoracion valorVenalParaAntiguedad:uso conValorDeReferencia:[acabado valueForKey:@"valorAcabado"]];    
    
    NSLog(@"minoración completa: %@", [[JMFactoresMinoracion valoresVenalesSucesivosParaValorDeReferencia:[acabado valueForKey:@"valorAcabado"]] description]);
    
    collapsed = YES;
        
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return JMTableNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // START:NumRows
    switch (section) {
        case JMTableSectionTasacion:
            return JMTableSecTasacionNumRows;
        case JMTableSectionFichaTecnica:
            if (collapsed) {
                return JMTableSecCollapsedFichaNumRows;
            }
            return JMTableSecFichaNumRows;
        default:
            NSLog(@"Unexpected section (%d)", section);
            break;
    }
    // END:NumRows
    return 0;
}

- (NSString *)marcaYModelo
{
    NSManagedObject *marca = (NSManagedObject *)tasacion.marca;
    NSManagedObject *acabado = (NSManagedObject *)tasacion.acabado;
    NSString *marcaString = [marca valueForKey:@"marca"];
    NSString *acabadoString = [acabado valueForKey:@"acabado"];
    return [NSString stringWithFormat:@"%@ - %@", marcaString, acabadoString];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == JMTableSectionFichaTecnica) {
        return @"Ficha técnica";
    } else if (section == JMTableSectionTasacion) {
        return [self marcaYModelo];
    }
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSManagedObject *object = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];

    // START:SearchingSection
    switch (indexPath.section) {
        case JMTableSectionTasacion: {
            cell = [JMCellStyleValue1 cellForTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.indentationLevel = 0;
            switch (indexPath.row) {
//                case JMTableSecTasacionRowMarca:
//                    cell.textLabel.text = NSLocalizedString(@"Marca", @"Marca");
//                    object = (NSManagedObject *)tasacion.marca;
//                    cell.detailTextLabel.text = [object valueForKey:@"marca"];
//                    break;
//                case JMTableSecTasacionRowAcabado:
//                    cell.textLabel.text = NSLocalizedString(@"Modelo", @"Modelo");
//                    object = (NSManagedObject *)tasacion.acabado;
//                    cell.detailTextLabel.text = [object valueForKey:@"acabado"];
//                    break;
                case JMTableSecTasacionRowFechaMatriculacion:
                    cell.textLabel.text = NSLocalizedString(@"Matriculación", @"Matriculación");
                    cell.textLabel.numberOfLines = 2;
                    cell.detailTextLabel.text = [dateFormatter stringFromDate:tasacion.fechaMatriculacion];
                    cell.detailTextLabel.contentMode = UIViewContentModeCenter;
                    break;
                case JMTableSecTasacionRowAntiguedad:
                    cell.textLabel.text = NSLocalizedString(@"Años de uso", @"Años de uso");
                    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                    cell.detailTextLabel.text = [numberFormatter stringFromNumber:tasacion.antiguedad];
                    break;
                case JMTableSecTasacionRowValorActual:
                    cell.textLabel.text = NSLocalizedString(@"Valor venal", @"Valor venal");
                    cell.detailTextLabel.text = [numberFormatter stringFromNumber:tasacion.precio];
                    break;
                default:
                    NSAssert1(NO, @"Unexpected row in Favorites section: %d",
                              indexPath.row);
                    break;
            }
            cell.accessoryType = UITableViewCellAccessoryNone;
            break;
        }
        case JMTableSectionFichaTecnica:
            cell = [JMCellStyleValue1 cellForTableView:tableView];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.indentationLevel = 0;
            object = (NSManagedObject *)tasacion.acabado; 
            if (indexPath.row < JMTableSecCollapsedFichaNumRows) {
                switch (indexPath.row) {
                    case JMTableSecCollapsedFichaRowPeriodoComercial:
                        cell.textLabel.text = NSLocalizedString(@"Periodo Comercial", @"Periodo Comercial");
                        cell.detailTextLabel.text = [object valueForKey:@"periodoComercial"];
                        break;
                    case JMTableSecCollapsedFichaRowCaballosVapor:
                        cell.textLabel.text = NSLocalizedString(@"Caballos de vapor", @"Caballos de vapor");
                        cell.detailTextLabel.text = [object valueForKey:@"caballosVapor"];
                        break;
                    case JMTableSecCollapsedFichaRowValorNuevo:
                        cell.textLabel.text = NSLocalizedString(@"Valor referencia", @"Valor referencia");
                        cell.detailTextLabel.text = [numberFormatter stringFromNumber:[object valueForKey:@"valorAcabado"]];
                        break;
                    case JMTableSecCollapsedFichaRowTitulo: {
                        // Create and configure the disclosure button.
                        cell.selectionStyle = UITableViewCellSelectionStyleGray;
                        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                        button.frame = CGRectMake(0.0, 5.0, 35.0, 35.0);
                        [button setImage:[UIImage imageNamed:@"carat.png"] forState:UIControlStateNormal];
                        [button setImage:[UIImage imageNamed:@"carat-open.png"] forState:UIControlStateSelected];
                        [button addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                        disclosureButton = button;
                        cell.accessoryView = disclosureButton;
                        
                        if (collapsed) {
                            cell.textLabel.text = NSLocalizedString(@"Más datos", @"Más datos");
                        } else {
                            cell.textLabel.text = NSLocalizedString(@"Menos datos", @"Menos datos");
                        }
                        break;
                    }
                    default:
                        NSAssert1(NO, @"Unexpected row in Colapsed section: %d",
                                  indexPath.row);
                        break;
                }
            }
            if (!collapsed && indexPath.row >= JMTableSecCollapsedFichaNumRows) {
                switch (indexPath.row) {
                    case JMTableSecFichaRowCilindrada:
                        cell.textLabel.text = NSLocalizedString(@"Cilindrada", @"Cilindrada");
                        cell.detailTextLabel.text = [object valueForKey:@"cubicaje"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowCilindros:
                        cell.textLabel.text = NSLocalizedString(@"Número de cilindros", @"Número de cilindros");
                        cell.detailTextLabel.text = [object valueForKey:@"cilindros"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowTipoCombustible:
                        cell.textLabel.text = NSLocalizedString(@"Tipo de combustible", @"Tipo de combustible");
                        cell.detailTextLabel.text = [object valueForKey:@"tipoCombustible"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowPotenciaKWh:
                        cell.textLabel.text = NSLocalizedString(@"Potencia en KW/h", @"Potencia en KW/h");
                        cell.detailTextLabel.text = [object valueForKey:@"potenciaKWH"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowCaballosFiscales:
                        cell.textLabel.text = NSLocalizedString(@"Caballos fiscales", @"Caballos fiscales");
                        cell.detailTextLabel.text = [object valueForKey:@"caballosFiscales"];
                        cell.indentationLevel = 1;
                        break;
                    case JMTableSecFichaRowEmisionCO2:
                        cell.textLabel.text = NSLocalizedString(@"Emisión CO2", @"Emisión CO2");
                        cell.detailTextLabel.text = [object valueForKey:@"emisionesCO2"];
                        cell.indentationLevel = 1;
                        break;
                    default:
                        NSAssert1(NO, @"Unexpected row in Elapsed section: %d",
                                  indexPath.row);
                        break;
                }
            }
            break;
        default:
            NSAssert1(NO, @"Unexpected section (%d)", indexPath.section);
            break;
            // END:SubmitSection
    }
    
    return cell;
}

- (void)accessoryButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
		
	{
        [self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    if (collapsed) {
        [self openSection];
    } else {
        [self closeSection];
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == JMTableSectionFichaTecnica && indexPath.row == JMTableSecCollapsedFichaRowTitulo && collapsed) {
        [self openSection];
    } else if (indexPath.section == JMTableSectionFichaTecnica && indexPath.row == JMTableSecCollapsedFichaRowTitulo && !collapsed) {
        [self closeSection];
    }
}

- (void)openSection
{
    self.disclosureButton.selected = !self.disclosureButton.selected;
    
    collapsed = NO;
    NSInteger countOfRowsToInsert = JMTableSecFichaNumRows;
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = JMTableSecCollapsedFichaNumRows; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:JMTableSectionFichaTecnica]];
    }
    
    // Apply the updates.
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:JMTableSecFichaRowEmisionCO2 inSection:JMTableSectionFichaTecnica] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

- (void)closeSection
{
    self.disclosureButton.selected = !self.disclosureButton.selected;
    
    collapsed = YES;
    NSInteger countOfRowsToDelete = JMTableSecFichaNumRows;
    NSMutableArray *indexPathToDelete = [[NSMutableArray alloc] init];
    for (NSInteger i = JMTableSecCollapsedFichaNumRows; i < countOfRowsToDelete; i++) {
        [indexPathToDelete addObject:[NSIndexPath indexPathForRow:i inSection:JMTableSectionFichaTecnica]];
    }
    
    // Apply the updates.
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:indexPathToDelete withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
}

- (void)save
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [self.tasacion managedObjectContext];
    [managedObjectContext save:&error];

    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
