//
//  Created by Björn Sållarp on 2009-06-14.
//  NO Copyright 2009 MightyLittle Industries. NO rights reserved.
// 
//  Use this code any way you like. If you do like it, please
//  link to my blog and/or write a friendly comment. Thank you!
//
//  Read my blog @ http://blog.sallarp.com
//

#import "CoreDataHelper.h"


@implementation CoreDataHelper


+(NSMutableArray *) searchObjectsInContext: (NSString*) entityName : (NSPredicate *) predicate : (NSString *) sortKey : (BOOL) sortAscending : (NSManagedObjectContext *) managedObjectContext
{

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
	[request setEntity:entity];	
	
	// If a predicate was passed, pass it to the query
	if(predicate != nil)
	{
		[request setPredicate:predicate];
	}
	
	// If a sort key was passed, use it for sorting.
	if(sortKey != nil)
	{		
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:sortAscending];
		NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];

		[request setSortDescriptors:sortDescriptors];
		[sortDescriptors release];
		[sortDescriptor release];
	}
	
	NSError *error;
	
	NSMutableArray *mutableFetchResults = [[[managedObjectContext executeFetchRequest:request error:&error] mutableCopy] autorelease];
	
	[request release];
	
	return mutableFetchResults;
}


+(NSMutableArray *) getObjectsFromContext: (NSString*) entityName : (NSString *) sortKey : (BOOL) sortAscending : (NSManagedObjectContext *) managedObjectContext
{
	return [self searchObjectsInContext:entityName :nil :sortKey :sortAscending :managedObjectContext];
}

+(NSMutableArray *) getObjectsFromContext:(NSString *)entityName withPredicate:(NSPredicate *)predicate inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    return [self searchObjectsInContext:entityName :predicate :nil :nil :managedObjectContext];
}

+(NSMutableArray *) searchObjectsInContext: (NSString*) entityName : (NSPredicate *) predicate : (NSMutableArray *) sortArray : (NSManagedObjectContext *) managedObjectContext
{
    
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
	[request setEntity:entity];	
	
	// If a predicate was passed, pass it to the query
	if(predicate != nil)
	{
		[request setPredicate:predicate];
	}
	
	// If a sort key was passed, use it for sorting.
	if(sortArray != nil)
	{
        NSSortDescriptor *sortDescriptor = nil;
		NSMutableArray *sortDescriptors = [[NSMutableArray alloc] initWithCapacity:0];
        for (int i = 0; i < [sortArray count]; i++) {
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:[sortArray objectAtIndex:i] ascending:YES];
            [sortDescriptors addObject:sortDescriptor];
        }
		[request setSortDescriptors:sortDescriptors];
		[sortDescriptors release];
		[sortDescriptor release];
	}
	
	NSError *error;
	
	NSMutableArray *mutableFetchResults = [[[managedObjectContext executeFetchRequest:request error:&error] mutableCopy] autorelease];
	
	[request release];
	
	return mutableFetchResults;
}


@end
