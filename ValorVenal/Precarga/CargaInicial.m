//
//  CargaInicial.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 20/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CargaInicial.h"

#import "Marcas.h"
#import "Modelos.h"
#import "Acabados.h"

@implementation CargaInicial

@synthesize managedObjectContext;

-(void)cargaMarcas {
	NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"ListaMarcas2012.csv"];
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath];
    
    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
	NSMutableArray *arrayVariable = [[NSMutableArray alloc] initWithArray:dataRows];
    NSLog(@"Numero de filas en dataRows Marcas: %i", [dataRows count]);
    
	for (int i = 0; i < [dataRows count]; i++) {
		NSArray *dataElements = [[arrayVariable objectAtIndex:i] componentsSeparatedByString:@";"];
        if ([dataElements count] >= 2){
            NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
            [formater setNumberStyle:NSNumberFormatterDecimalStyle];
            //			NSNumber *idRow = [formater numberFromString:[dataElements objectAtIndex:0]];
            
			Marcas *marcasReg = (Marcas *)[NSEntityDescription insertNewObjectForEntityForName:@"Marcas" inManagedObjectContext:self.managedObjectContext];
			[marcasReg setMarca:[dataElements objectAtIndex:0]];
			[marcasReg setIdMarca:[dataElements objectAtIndex:1]];

            NSLog(@"Todo ha ido correctamente para Marcas %i", i);            
		} 
	}
    NSError *errorSave;			
    if (![self.managedObjectContext save:&errorSave]) {
        NSLog(@"Unresolved Core Data Save error %@, %@", errorSave, [errorSave userInfo]);
        exit(-1);
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:@"Grabacion de datos" 
                              message:@"Se ha producido un error" 
                              delegate:self 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil];
        [alert show];
    } 
}	

-(void)cargaModelos {
	NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"ListaModelos2012.csv"];
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath];
    
    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
	NSMutableArray *arrayVariable = [[NSMutableArray alloc] initWithArray:dataRows];
    NSLog(@"Numero de filas en dataRows Modelos: %i", [dataRows count]);
    
	for (int i = 0; i < [dataRows count]; i++) {
		NSArray *dataElements = [[arrayVariable objectAtIndex:i] componentsSeparatedByString:@";"];
        if ([dataElements count] >= 3){
            NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
            [formater setNumberStyle:NSNumberFormatterDecimalStyle];
            
            NSString *preparaIdModelo = [NSString stringWithFormat:@"%@-%@", [dataElements objectAtIndex:0], [dataElements objectAtIndex:2]];
            
			Modelos *modelosReg = (Modelos *)[NSEntityDescription insertNewObjectForEntityForName:@"Modelos" inManagedObjectContext:self.managedObjectContext];
            [modelosReg setIdModelo:preparaIdModelo];
			[modelosReg setModelo:[dataElements objectAtIndex:1]];
            [modelosReg setMarca:[self recuperaMarca:[dataElements objectAtIndex:0]]];
            
            NSLog(@"Todo ha ido correctamente para Modelos %i", i);
		} 
	}
    NSError *errorSave;			
    if (![self.managedObjectContext save:&errorSave]) {
        NSLog(@"Unresolved Core Data Save error %@, %@", errorSave, [errorSave userInfo]);
        exit(-1);
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:@"Grabacion de datos" 
                              message:@"Se ha producido un error" 
                              delegate:self 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil];
        [alert show];
    }
}	

-(void)cargaAcabados {
	NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"ListaPrecios2012.csv"];
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath];
    
    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
	NSMutableArray *arrayVariable = [[NSMutableArray alloc] initWithArray:dataRows];
    NSLog(@"Numero de filas en dataRows Acabados: %i", [dataRows count]);
    
	for (int i = 0; i < [dataRows count]; i++) {
		NSArray *dataElements = [[arrayVariable objectAtIndex:i] componentsSeparatedByString:@";"];
        if ([dataElements count] >= 3){
            NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
            [formater setNumberStyle:NSNumberFormatterDecimalStyle];
            
//            NSString *tipoCombustible = nil;
//            if ([[dataElements objectAtIndex:6] isEqualToString:@"G"]) {
//                tipoCombustible = @"GASOLINA";
//            } else if ([[dataElements objectAtIndex:6] isEqualToString:@"D"]) {
//                tipoCombustible = @"DIESEL";
//            } else if ([[dataElements objectAtIndex:6] isEqualToString:@"M"]) {
//                tipoCombustible = @"BIOETANOL O GASOLINA";
//            } else if ([[dataElements objectAtIndex:6] isEqualToString:@"S"]) {
//                tipoCombustible = @"GASOLINA GLP";
//            } else if ([[dataElements objectAtIndex:6] isEqualToString:@"Elec"]) {
//                tipoCombustible = @"ELECTRICO";
//            } else {
//                NSLog(@"Tipo de combustible no encontrado: %@", [dataElements objectAtIndex:6]);
//            }
//            
			Acabados *acabadosReg = (Acabados *)[NSEntityDescription insertNewObjectForEntityForName:@"Acabados" inManagedObjectContext:self.managedObjectContext];
            [acabadosReg setMarca:[self recuperaMarca:[dataElements objectAtIndex:0]]];
			[acabadosReg setIdAcabado:[dataElements objectAtIndex:1]];
			[acabadosReg setAcabado:[dataElements objectAtIndex:2]];
			[acabadosReg setPeriodoComercial:[dataElements objectAtIndex:3]];
			[acabadosReg setCubicaje:[dataElements objectAtIndex:4]];
			[acabadosReg setCilindros:[dataElements objectAtIndex:5]];
			[acabadosReg setTipoCombustible:[dataElements objectAtIndex:6]];
			[acabadosReg setPotenciaKWH:[dataElements objectAtIndex:7]];
			[acabadosReg setCaballosFiscales:[dataElements objectAtIndex:8]];
			[acabadosReg setEmisionesCO2:[dataElements objectAtIndex:9]];
			[acabadosReg setCaballosVapor:[dataElements objectAtIndex:10]];
			[acabadosReg setValorAcabado:[formater numberFromString:[dataElements objectAtIndex:11]]];
			[acabadosReg setVentaDesde:[formater numberFromString:[dataElements objectAtIndex:12]]];
			[acabadosReg setVentaHasta:[formater numberFromString:[dataElements objectAtIndex:13]]];
            
            NSLog(@"Todo ha ido correctamente para Acabados %i", i);
		} 
	}
    NSError *errorSave;			
    if (![self.managedObjectContext save:&errorSave]) {
        NSLog(@"Unresolved Core Data Save error %@, %@", errorSave, [errorSave userInfo]);
        exit(-1);
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:@"Grabacion de datos" 
                              message:@"Se ha producido un error" 
                              delegate:self 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil];
        [alert show];
    }
}	

-(void)cargaMatching {
	NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"ListaMatcheo2012.csv"];
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath];
    
    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
	NSMutableArray *arrayVariable = [[NSMutableArray alloc] initWithArray:dataRows];
    NSLog(@"Numero de filas en dataRows Matching: %i", [dataRows count]);
    
	for (int i = 0; i < [dataRows count]; i++) {
		NSArray *dataElements = [[arrayVariable objectAtIndex:i] componentsSeparatedByString:@";"];
        if ([dataElements count] >= 3){
            NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
            [formater setNumberStyle:NSNumberFormatterDecimalStyle];
            
			Acabados *acabadosReg = [self recuperaAcabado:[dataElements objectAtIndex:0]];
            NSString *preparaIdModelo = [NSString stringWithFormat:@"%@-%@", [dataElements objectAtIndex:1], [dataElements objectAtIndex:2]];
            [acabadosReg addModelosObject:[self recuperaModelo:preparaIdModelo]];
            
            NSLog(@"Todo ha ido correctamente para Matching %i", i);
		} 
	}
    NSError *errorSave;			
    if (![self.managedObjectContext save:&errorSave]) {
        NSLog(@"Unresolved Core Data Save error %@, %@", errorSave, [errorSave userInfo]);
        exit(-1);
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:@"Grabacion de datos" 
                              message:@"Se ha producido un error" 
                              delegate:self 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil];
        [alert show];
    }
}	

-(void)cargaMatriculasActuales {
	NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"MatriculasActuales.csv"];
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath];
    
    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
	NSMutableArray *arrayVariable = [[NSMutableArray alloc] initWithArray:dataRows];
    NSLog(@"Numero de filas en dataRows Matriculas Actuales: %i", [dataRows count]);
    
	for (int i = 0; i < [dataRows count]; i++) {
		NSArray *dataElements = [[arrayVariable objectAtIndex:i] componentsSeparatedByString:@";"];
        if ([dataElements count] >= 3){
            NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
            [formater setNumberStyle:NSNumberFormatterDecimalStyle];
            NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
            [dateFormater setDateFormat:@"dd/MM/yyyy"];
            
			Matriculas *matriculasReg = (Matriculas *)[NSEntityDescription insertNewObjectForEntityForName:@"Matriculas" inManagedObjectContext:self.managedObjectContext];
            [matriculasReg setTipoMatricula:@"3"];
			[matriculasReg setZona:@"ESP"];
            [matriculasReg setFechaCorte:[dateFormater dateFromString:[dataElements objectAtIndex:0]]];
            [matriculasReg setLetras:[dataElements objectAtIndex:1]];
            [matriculasReg setNumeros:[formater numberFromString:[dataElements objectAtIndex:2]]];
            
            NSLog(@"Todo ha ido correctamente para Matriculas Actuales %i", i);
		} 
	}
    NSError *errorSave;			
    if (![self.managedObjectContext save:&errorSave]) {
        NSLog(@"Unresolved Core Data Save error %@, %@", errorSave, [errorSave userInfo]);
        exit(-1);
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:@"Grabacion de datos" 
                              message:@"Se ha producido un error" 
                              delegate:self 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil];
        [alert show];
    }
}	

-(void)cargaMatriculasAntiguas {
	NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"MatriculasAntiguas.csv"];
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath];
    
    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
	NSMutableArray *arrayVariable = [[NSMutableArray alloc] initWithArray:dataRows];
    NSLog(@"Numero de filas en dataRows Matriculas Antiguas: %i", [dataRows count]);
    
	for (int i = 1; i < [dataRows count]; i++) {
		NSArray *arrayZonas = [[arrayVariable objectAtIndex:0] componentsSeparatedByString:@";"];
		NSArray *dataElements = [[arrayVariable objectAtIndex:i] componentsSeparatedByString:@";"];
		for (int j = 1; j < [dataElements count]; j++) {
            NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
            [formater setNumberStyle:NSNumberFormatterDecimalStyle];
            NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
            [dateFormater setDateFormat:@"dd/MM/yyyy"];
            
            NSArray *arrayMatricula = [[dataElements objectAtIndex:j] componentsSeparatedByString:@"-"];
            
			Matriculas *matriculasReg = (Matriculas *)[NSEntityDescription insertNewObjectForEntityForName:@"Matriculas" inManagedObjectContext:self.managedObjectContext];
            [matriculasReg setTipoMatricula:@"2"];
			[matriculasReg setZona:[arrayZonas objectAtIndex:j]];
            [matriculasReg setFechaCorte:[dateFormater dateFromString:[dataElements objectAtIndex:0]]];
            [matriculasReg setLetras:[arrayMatricula objectAtIndex:1]];
            [matriculasReg setNumeros:[formater numberFromString:[arrayMatricula objectAtIndex:0]]];
            
            NSLog(@"Todo ha ido correctamente para Matriculas Antiguas %i", i);
		}
	}
    NSError *errorSave;			
    if (![self.managedObjectContext save:&errorSave]) {
        NSLog(@"Unresolved Core Data Save error %@, %@", errorSave, [errorSave userInfo]);
        exit(-1);
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:@"Grabacion de datos" 
                              message:@"Se ha producido un error" 
                              delegate:self 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil];
        [alert show];
    }
}	

-(void)cargaMatriculasMasAntiguas {
	NSString *paths = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [paths stringByAppendingPathComponent:@"MatriculasMasAntiguas.csv"];
    NSString *dataFile = [[NSString alloc] initWithContentsOfFile:bundlePath];
    
    NSArray *dataRows = [dataFile componentsSeparatedByString:@"\r"];
	NSMutableArray *arrayVariable = [[NSMutableArray alloc] initWithArray:dataRows];
    NSLog(@"Numero de filas en dataRows Matriculas Mas Antiguas: %i", [dataRows count]);
    
	for (int i = 1; i < [dataRows count]; i++) {
		NSArray *arrayZonas = [[arrayVariable objectAtIndex:0] componentsSeparatedByString:@";"];
		NSArray *dataElements = [[arrayVariable objectAtIndex:i] componentsSeparatedByString:@";"];
		for (int j = 1; j < [dataElements count]; j++) {
            NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
            [formater setNumberStyle:NSNumberFormatterDecimalStyle];
            NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
            [dateFormater setDateFormat:@"dd/MM/yyyy"];
            
			Matriculas *matriculasReg = (Matriculas *)[NSEntityDescription insertNewObjectForEntityForName:@"Matriculas" inManagedObjectContext:self.managedObjectContext];
            [matriculasReg setTipoMatricula:@"1"];
			[matriculasReg setZona:[arrayZonas objectAtIndex:j]];
            [matriculasReg setFechaCorte:[dateFormater dateFromString:[dataElements objectAtIndex:0]]];
            [matriculasReg setLetras:nil];
            [matriculasReg setNumeros:[formater numberFromString:[dataElements objectAtIndex:j]]];
            
            NSLog(@"Todo ha ido correctamente para Matriculas Mas Antiguas %i", i);
		}
	}
    NSError *errorSave;			
    if (![self.managedObjectContext save:&errorSave]) {
        NSLog(@"Unresolved Core Data Save error %@, %@", errorSave, [errorSave userInfo]);
        exit(-1);
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:@"Grabacion de datos" 
                              message:@"Se ha producido un error" 
                              delegate:self 
                              cancelButtonTitle:@"Cancel" 
                              otherButtonTitles:nil];
        [alert show];
    }
}	

-(Marcas *)recuperaMarca:(NSString *)idMarca {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Marcas" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idMarca=%@",idMarca];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *arrayFetchRequest = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([arrayFetchRequest count] >= 1) {
        return [arrayFetchRequest objectAtIndex:0];
    } else {
        return nil;
    }    
}

-(Modelos *)recuperaModelo:(NSString *)idModelo {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Modelos" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idModelo=%@",idModelo];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *arrayFetchRequest = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([arrayFetchRequest count] >= 1) {
        return [arrayFetchRequest objectAtIndex:0];
    } else {
        return nil;
    }    
}

-(Acabados *)recuperaAcabado:(NSString *)idAcabado {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Acabados" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idAcabado=%@",idAcabado];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *arrayFetchRequest = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([arrayFetchRequest count] >= 1) {
        return [arrayFetchRequest objectAtIndex:0];
    } else {
        return nil;
    }    
}

@end
