//
//  CargaInicial.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 20/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Marcas.h"
#import "Modelos.h"
#import "Acabados.h"
#import "Matriculas.h"

@interface CargaInicial : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

-(void)cargaMarcas;
-(void)cargaModelos;
-(void)cargaAcabados;
-(void)cargaMatching;
-(void)cargaMatriculasActuales;
-(void)cargaMatriculasAntiguas;
-(void)cargaMatriculasMasAntiguas;

-(Marcas *)recuperaMarca:(NSString *)idMarca;
-(Modelos *)recuperaModelo:(NSString *)idModelo;
-(Acabados *)recuperaAcabado:(NSString *)idAcabado;

@end
