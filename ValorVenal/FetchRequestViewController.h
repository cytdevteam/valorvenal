//
//  CalendariosViewController.h
//  UtilBB
//
//  Created by Javier Moreno Lozano on 26/10/10.
//  Copyright (c) 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"
#import "MBProgressHUD.h"

@interface FetchRequestViewController : UITableViewController  {

@private
	NSManagedObjectContext *managedObjectContext;
    
    NSString *entityName;
    NSString *orderKey;
    NSMutableArray *orderArray;
    NSArray *entityArray;
    NSPredicate *entitySearchPredicate;
    NSString *titleNavBar;
    MBProgressHUD *HUD;
    
    int alturaCelda;
    
}

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, retain) NSString *entityName;
@property (nonatomic, retain) NSString *orderKey;
@property (nonatomic, retain) NSMutableArray *orderArray;
@property (nonatomic, retain) NSArray *entityArray;
@property (nonatomic, retain) NSPredicate *entitySearchPredicate;
@property (nonatomic, retain) NSString *titleNavBar;
@property (nonatomic, retain) MBProgressHUD *HUD;

- (IBAction)showSimple:(id)sender;
- (IBAction)hideSimple:(id)sender;

@end