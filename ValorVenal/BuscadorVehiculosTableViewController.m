//
//  BuscadorVehiculosTableViewController.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 02/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BuscadorVehiculosTableViewController.h"
#import "JMCellStyleValue1.h"
#import "JMCellInputDate.h"
#import "JMCellStyleButton.h"
#import "JMObjectSelectionViewController.h"
#import "Tasaciones.h"

#import "FetchRequestViewController.h"
#import "TasacionesTableViewController.h"
#import "InfoViewController.h"
#import "CoreDataHelper.h"

#import "InAppTasaCocheIAPHelper.h"

// START:TableSections
enum JMTableSections {
    JMTableSectionSearching = 0,
    JMTableSectionSubmit,
    JMTableNumSections,
};
// END:TableSections

// START:TableRows
enum JMSearchingRows {
    JMTableSecSearchingRowFechaMatriculacion = 0,
    JMTableSecSearchingRowMarca,
    JMTableSecSearchingRowModelo,
    JMTableSecSearchingRowCombustible,
    JMTableSecSearchingRowCilindrada,
    JMTableSecSearchingRowPotencia,
    JMTableSecSearchingNumRows,
};

enum JMMandatoryRows {
    JMTableSecMandatoryRowFechaMatriculacion = 0,
    JMTableSecMandatoryRowMarca,
    JMTableSecMandatoryRowModelo,
    JMTableSecMandatoryRowCombustible,
    JMTableSecMandatoryNumRows,
};

enum JMSubmitRows {
    JMTableSecSubmitRowSubmits = 0,
    JMTableSecSubmitNumRows,
};
// END:TableRows

BOOL isProVersion;

@implementation BuscadorVehiculosTableViewController 

@synthesize tableView = _tableView;
@synthesize selectionViewController;
@synthesize managedObjectContext;
@synthesize tasacion;
@synthesize dateFormatter;
@synthesize HUD;
@synthesize bannerView = _bannerView;
@synthesize admobBannerView = _admobBannerView;

- (void)didReceiveMemoryWarning
{ 
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoButton addTarget:self action:@selector(showInfoViewController) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    self.navigationItem.leftBarButtonItem = infoButtonItem;
    
    UIBarButtonItem *actionButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showActionSheet)];
    self.navigationItem.rightBarButtonItem = actionButtonItem;
    
    self.navigationController.title = NSLocalizedString(@"Nueva tasación", @"Nueva tasación");
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor underPageBackgroundColor];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
	[self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{

    isProVersion = [InAppTasaCocheIAPHelper isProVersion];
    
    //a partir de aquí vienen los metodos de iAd:
    if (!isProVersion) {
        self.bannerView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, 366, 320, 50)];
        self.bannerView.backgroundColor = [UIColor clearColor];
        [self.bannerView setRequiredContentSizeIdentifiers:[NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait, nil]];
        self.bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
        [self.bannerView setDelegate:self];
        [self.view addSubview:self.bannerView];        
    } else {
        if (self.bannerView) {
            [self.bannerView removeFromSuperview];
        }
        if (self.admobBannerView) {
            [self.admobBannerView removeFromSuperview];   
        }
    }
    
    if (![self.tasacion isInserted]) {
        self.tasacion = nil;
        self.tasacion = [NSEntityDescription insertNewObjectForEntityForName:@"Tasaciones" inManagedObjectContext:self.managedObjectContext];
    }
    [super viewWillAppear:animated];
    [self.tableView reloadData];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return JMTableNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // START:NumRows
    switch (section) {
        case JMTableSectionSearching:
            return JMTableSecSearchingNumRows;
        case JMTableSectionSubmit:
            return JMTableSecSubmitNumRows;
        default:
            NSLog(@"Unexpected section (%d)", section);
            break;
    }
    // END:NumRows
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (section == JMTableSectionSearching) {
        return NSLocalizedString(@"Campos con * son obligatorios", @"Campos con * son obligatorios");
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSManagedObject *object = nil;
    
    // START:SearchingSection
    switch (indexPath.section) {
        case JMTableSectionSearching:
            cell = [JMCellStyleValue1 cellForTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            switch (indexPath.row) {
                case JMTableSecSearchingRowMarca:
                    cell.textLabel.text = NSLocalizedString(@"Marca *", @"Marca *");
                    object = (NSManagedObject *)self.tasacion.marca;
                    cell.detailTextLabel.text = [object valueForKey:@"marca"];
                    break;
                case JMTableSecSearchingRowModelo:
                    cell.textLabel.text = NSLocalizedString(@"Modelo *", @"Modelo *");
                    object = (NSManagedObject *)self.tasacion.modelo;
                    cell.detailTextLabel.text = [object valueForKey:@"modelo"];
                    break;
                case JMTableSecSearchingRowFechaMatriculacion:
                    cell.textLabel.text = NSLocalizedString(@"Matriculación *", @"Matriculación *");
                    cell.textLabel.numberOfLines = 2;
                    cell.detailTextLabel.text = [self.dateFormatter stringFromDate:self.tasacion.fechaMatriculacion];
                    cell.detailTextLabel.contentMode = UIViewContentModeCenter;
                    break;
                case JMTableSecSearchingRowCombustible:
                    cell.textLabel.text = NSLocalizedString(@"Combustible *", @"Combustible *");
                    cell.detailTextLabel.text = self.tasacion.combustible;
                    break;
                case JMTableSecSearchingRowCilindrada:
                    cell.textLabel.text = NSLocalizedString(@"Cilindrada", @"Cilindrada");
                    cell.detailTextLabel.text = self.tasacion.cubicaje;
                    break;
                case JMTableSecSearchingRowPotencia:
                    cell.textLabel.text = NSLocalizedString(@"Potencia", @"Potencia");
                    cell.detailTextLabel.text = self.tasacion.potencia;
                    break;
                default:
                    NSAssert1(NO, @"Unexpected row in Favorites section: %d",
                              indexPath.row);
                    break;
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
            // END:SearchingSection
            
            // START:SubmitSection
        case JMTableSectionSubmit:
            cell = [JMCellStyleButton cellForTableView:tableView];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            switch (indexPath.row) {
                case JMTableSecSubmitRowSubmits: 
                    ((JMCellStyleButton *)cell).title = NSLocalizedString(@"Buscar", @"Buscar");
                    ((JMCellStyleButton *)cell).color = [UIColor redColor];
                    break;
                default:
                    NSAssert1(NO, @"Unexpected row in Alerts section: %d",
                              indexPath.row);
                    break;
            }
            break;
        default:
            NSAssert1(NO, @"Unexpected section (%d)", indexPath.section);
            break;
            // END:SubmitSection
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)comprobacionesTrasLaSeleccionConIndexPath:(NSIndexPath *)indexPath
{
    BOOL pasaValidacion = YES;
    UIViewController *viewController = nil;
    if (indexPath.section == JMTableSectionSearching && pasaValidacion) {
        if ([self informaAntesDeLaFila:indexPath.row]) {
            pasaValidacion = NO;
            UIAlertView *alertView = [[UIAlertView alloc]  
                                      initWithTitle:nil 
                                      message:[self informaAntesDeLaFila:indexPath.row] 
                                      delegate:self
                                      cancelButtonTitle:@"Ok" 
                                      otherButtonTitles:nil];
            [alertView show];
        }
        if ([self avisoDeLimpiezaPorCambioEnFila:indexPath.row] && pasaValidacion) {
            UIAlertView *alertView = [[UIAlertView alloc]  
                                      initWithTitle:nil
                                      message:NSLocalizedString(@"La modificación de este valor puede hacer que pierda la información seleccionadas", @"La modificación de este valor puede hacer que pierda la información seleccionadas") 
                                      delegate:self
                                      cancelButtonTitle:@"Cancel" 
                                      otherButtonTitles:@"Ok", nil];
            alertView.tag = 1;
            [alertView show];
        } else if (pasaValidacion) {
            viewController = [self viewControllerForSearchingSelection:indexPath];
        }
    } else if (indexPath.section == JMTableSectionSubmit && pasaValidacion) {
        if ([self validaObligatorios]) {
            pasaValidacion = NO;
            UIAlertView *alertView = [[UIAlertView alloc]  
                                      initWithTitle:nil
                                      message:[self validaObligatorios]
                                      delegate:self 
                                      cancelButtonTitle:@"Ok" 
                                      otherButtonTitles:nil];
            [alertView show];
        } 
        if (pasaValidacion) {
            viewController = [self viewControllerForSubmittingSelection:indexPath];
        }
    }
    if (viewController && pasaValidacion) {
        [self.navigationController pushViewController:viewController animated:YES];
    }    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != JMTableSecSearchingRowFechaMatriculacion) {
        
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        HUD.delegate = self;
        
        [HUD showWhileExecuting:@selector(comprobacionesTrasLaSeleccionConIndexPath:) onTarget:self withObject:indexPath animated:YES];
        
    } else {
        [self comprobacionesTrasLaSeleccionConIndexPath:indexPath];
    }
    

//    if (!pasaValidacion) {
//        [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex != [alertView cancelButtonIndex]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        UIViewController *viewController = [self viewControllerForSearchingSelection:indexPath];
        [self.navigationController pushViewController:viewController animated:YES];
        NSLog(@"El indexpath en el que se encuentra ahora es: %@", [indexPath description]);
    } else if (alertView.tag == 2) {
        [self DatePickerView];
    } else{
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    }
    
}

- (BOOL)fechaDeMatriculacionEsCorrecta
{
    return ([self.tasacion.fechaMatriculacion compare:[NSDate date]] == NSOrderedAscending);
}

- (UIViewController *)viewControllerForSearchingSelection:(NSIndexPath *)indexPath
{
    UIViewController *viewController = [[JMObjectSelectionViewController alloc] initWithStyle:UITableViewStyleGrouped];
    switch (indexPath.row) {
        case JMTableSecSearchingRowMarca:
            NSLog(@"Marca row selected");
            ((JMObjectSelectionViewController *)viewController).entityName = @"Marcas";
            ((JMObjectSelectionViewController *)viewController).titleProperty = @"marca";
            ((JMObjectSelectionViewController *)viewController).titleNavBar = @"Marcas";
            ((JMObjectSelectionViewController *)viewController).managedObjectContext = managedObjectContext;
            ((JMObjectSelectionViewController *)viewController).managedObject = self.tasacion;
            ((JMObjectSelectionViewController *)viewController).objectSelected = @"marca";
            ((JMObjectSelectionViewController *)viewController).indexPath = indexPath;
            ((JMObjectSelectionViewController *)viewController).delegate = self;
            if ([(JMObjectSelectionViewController *)viewController requestWithEmptyArrayForObjectSelected]) {
                [self requestWithEmptyArrayForObjectSelected:nil];
                return nil;
            }
            break;
        case JMTableSecSearchingRowModelo:
            NSLog(@"Modelo row selected");
            ((JMObjectSelectionViewController *)viewController).entityName = @"Modelos";
            ((JMObjectSelectionViewController *)viewController).titleProperty = @"modelo";
            ((JMObjectSelectionViewController *)viewController).titleNavBar = @"Modelos";
            ((JMObjectSelectionViewController *)viewController).managedObjectContext = managedObjectContext;
            ((JMObjectSelectionViewController *)viewController).managedObject = self.tasacion;
            ((JMObjectSelectionViewController *)viewController).objectSelected = @"modelo";
            ((JMObjectSelectionViewController *)viewController).indexPath = indexPath;
            ((JMObjectSelectionViewController *)viewController).entitySearchPredicate = [self predicateForModelos];
            ((JMObjectSelectionViewController *)viewController).delegate = self;
            if ([(JMObjectSelectionViewController *)viewController requestWithEmptyArrayForObjectSelected]) {
                [self requestWithEmptyArrayForObjectSelected:nil];
                return nil;
            }
            break;
        case JMTableSecSearchingRowFechaMatriculacion:
            viewController = nil;
            [self DatePickerView];
            break;
        case JMTableSecSearchingRowCombustible:
            NSLog(@"Combustible row selected");
            ((JMObjectSelectionViewController *)viewController).entityName = @"Acabados";
            ((JMObjectSelectionViewController *)viewController).titleProperty = @"tipoCombustible";
            ((JMObjectSelectionViewController *)viewController).titleNavBar = @"Combustible";
            ((JMObjectSelectionViewController *)viewController).managedObjectContext = managedObjectContext;
            ((JMObjectSelectionViewController *)viewController).managedObject = self.tasacion;
            ((JMObjectSelectionViewController *)viewController).objectSelected = @"combustible";
            ((JMObjectSelectionViewController *)viewController).groupBy = @"@distinctUnionOfObjects.tipoCombustible";
            ((JMObjectSelectionViewController *)viewController).indexPath = indexPath;
            ((JMObjectSelectionViewController *)viewController).entitySearchPredicate = [self predicateForAcabadosWithRow:indexPath];
            ((JMObjectSelectionViewController *)viewController).delegate = self;
            if ([(JMObjectSelectionViewController *)viewController requestWithEmptyArrayForObjectSelected]) {
                [self requestWithEmptyArrayForObjectSelected:nil];
                return nil;
            }
            break;
        case JMTableSecSearchingRowCilindrada:
            NSLog(@"Cilindrada row selected");
            ((JMObjectSelectionViewController *)viewController).entityName = @"Acabados";
            ((JMObjectSelectionViewController *)viewController).titleProperty = @"cubicaje";
            ((JMObjectSelectionViewController *)viewController).titleNavBar = @"Cilindrada";
            ((JMObjectSelectionViewController *)viewController).managedObjectContext = managedObjectContext;
            ((JMObjectSelectionViewController *)viewController).managedObject = self.tasacion;
            ((JMObjectSelectionViewController *)viewController).objectSelected = @"cubicaje";
            ((JMObjectSelectionViewController *)viewController).indexPath = indexPath;
            ((JMObjectSelectionViewController *)viewController).groupBy = @"@distinctUnionOfObjects.cubicaje";
            ((JMObjectSelectionViewController *)viewController).entitySearchPredicate = [self predicateForAcabadosWithRow:indexPath];
            ((JMObjectSelectionViewController *)viewController).delegate = self;
            if ([(JMObjectSelectionViewController *)viewController requestWithEmptyArrayForObjectSelected]) {
                [self requestWithEmptyArrayForObjectSelected:nil];
                return nil;
            }
            break;
        case JMTableSecSearchingRowPotencia:
            NSLog(@"Potencia row selected");
            ((JMObjectSelectionViewController *)viewController).entityName = @"Acabados";
            ((JMObjectSelectionViewController *)viewController).titleProperty = @"caballosVapor";
            ((JMObjectSelectionViewController *)viewController).titleNavBar = @"Potencia";
            ((JMObjectSelectionViewController *)viewController).managedObjectContext = managedObjectContext;
            ((JMObjectSelectionViewController *)viewController).managedObject = self.tasacion;
            ((JMObjectSelectionViewController *)viewController).objectSelected = @"potencia";
            ((JMObjectSelectionViewController *)viewController).indexPath = indexPath;
            ((JMObjectSelectionViewController *)viewController).groupBy = @"@distinctUnionOfObjects.caballosVapor";
            ((JMObjectSelectionViewController *)viewController).entitySearchPredicate = [self predicateForAcabadosWithRow:indexPath];
            ((JMObjectSelectionViewController *)viewController).delegate = self;
            if ([(JMObjectSelectionViewController *)viewController requestWithEmptyArrayForObjectSelected]) {
                [self requestWithEmptyArrayForObjectSelected:nil];
                return nil;
            }
            break;
        default:
            NSAssert1(NO, @"Unexpected selection of row %d", indexPath.row);
            break;            
    }
    return viewController;
}

- (UIViewController *)viewControllerForSubmittingSelection:(NSIndexPath *)indexPath
{
    UIViewController *viewController = [[JMObjectSelectionViewController alloc] initWithStyle:UITableViewStyleGrouped];
    switch (indexPath.row) {
        case JMTableSecSubmitRowSubmits:
            NSLog(@"Submit row selected");
            ((JMObjectSelectionViewController *)viewController).entityName = @"Acabados";
            ((JMObjectSelectionViewController *)viewController).titleProperty = @"acabado";
            ((JMObjectSelectionViewController *)viewController).subtitleProperty = @"periodoComercial";
            ((JMObjectSelectionViewController *)viewController).orderArray = [[NSMutableArray alloc] initWithObjects:@"periodoComercial", @"acabado", nil];
            ((JMObjectSelectionViewController *)viewController).titleNavBar = @"Modelo-Tipo";
            ((JMObjectSelectionViewController *)viewController).managedObjectContext = managedObjectContext;
            ((JMObjectSelectionViewController *)viewController).managedObject = self.tasacion;
            ((JMObjectSelectionViewController *)viewController).objectSelected = @"acabado";
            ((JMObjectSelectionViewController *)viewController).indexPath = indexPath;
            ((JMObjectSelectionViewController *)viewController).groupBy = nil;
            ((JMObjectSelectionViewController *)viewController).entitySearchPredicate = [self predicateForAcabadosWithRow:indexPath];
            ((JMObjectSelectionViewController *)viewController).delegate = self;
            if ([(JMObjectSelectionViewController *)viewController requestWithEmptyArrayForObjectSelected]) {
                [self requestWithEmptyArrayForObjectSelected:nil];
                return nil;
            }
            break;
        default:
            NSAssert1(NO, @"Unexpected selection of row %d", indexPath.row);
            break;            
    }

    return viewController;
}

- (void)requestWithEmptyArrayForObjectSelected:(NSString *)objectSelected
{
    [self hideSimple:nil];
    [self.navigationController popViewControllerAnimated:YES];
    UIAlertView *alertView = [[UIAlertView alloc]  
                              initWithTitle:nil
                              message:@"No se han encontrado vehículos. Verifique fecha de primera matriculación, marca o modelo" 
                              delegate:self
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil];
    [alertView show];
    NSLog(@"No hay ningún dato que mostrar");
}




#pragma mark - Mis cosas

- (NSPredicate *)predicateForModelos
{
    NSPredicate *predicate = nil;
    
    predicate = [NSPredicate predicateWithFormat:@"(marca == %@)", self.tasacion.marca];
    
    return predicate;
}

- (NSPredicate *)predicateForModelComprobation
{
    NSPredicate *predicate = nil;
    
    NSManagedObject *modelo = self.tasacion.modelo;
    
    predicate = [NSPredicate predicateWithFormat:@"(marca == %@) && (modelo == %@)", self.tasacion.marca, [modelo valueForKey:@"modelo"]];
    
    return predicate;
}

- (NSPredicate *)predicateForAcabadosWithRow:(NSIndexPath *)indexPath
{
    NSMutableArray *predicateArray = [NSMutableArray array];
    int iMax;
    if (indexPath.section == JMTableSectionSearching) {
        iMax = indexPath.row;
    } else {
        iMax = JMTableSecSearchingNumRows;
    }
    for (int i = 0; i < iMax; i++) {
        if (self.tasacion.marca && i == JMTableSecSearchingRowMarca) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"(marca == %@)", self.tasacion.marca]];
        }
        if (self.tasacion.modelo && i == JMTableSecSearchingRowModelo) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"(modelos CONTAINS %@)", self.tasacion.modelo]];
        }
        if (self.tasacion.fechaMatriculacion && i == JMTableSecSearchingRowFechaMatriculacion) {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:self.tasacion.fechaMatriculacion];
            NSInteger year = [components year];
            NSInteger ventaDesde = year - 0;
            NSInteger ventaHasta = year + 0;
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"(ventaDesde <=  %i)", ventaDesde]];
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"(ventaHasta >= %i)", ventaHasta]];
        }
        if (self.tasacion.combustible && i == JMTableSecSearchingRowCombustible) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"(tipoCombustible == %@)", self.tasacion.combustible]];
        }
        if (self.tasacion.cubicaje && i == JMTableSecSearchingRowCilindrada) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"(cubicaje == %@)", self.tasacion.cubicaje]];
        }
        if (self.tasacion.potencia && i == JMTableSecSearchingRowPotencia) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"(caballosVapor == %@)", self.tasacion.potencia]];
        }

    }
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
    return compoundPredicate;
}

- (void)showInfoViewController
{
    InfoViewController *viewController =  [[InfoViewController alloc] initWithStyle:UITableViewStyleGrouped];
    viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    UINavigationController *infoNavigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.navigationController presentModalViewController:infoNavigationController animated:YES];
}

- (void)showActionSheet
{
    NSMutableArray *buttonsArray = [[NSMutableArray alloc] initWithCapacity:0];
    if (isProVersion) {
        buttonsArray = [NSMutableArray arrayWithObjects:
                        [NSString stringWithString:NSLocalizedString(@"Marcas y modelos", @"Marcas y modelos")], 
                        [NSString stringWithString:NSLocalizedString(@"Historial de tasaciones", @"Historial de tasaciones")], 
                        nil];
    }
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] 
                                  initWithTitle:NSLocalizedString(@"Seleccione una opción", @"Seleccione una opción") delegate:self 
                                  cancelButtonTitle:nil
                                  destructiveButtonTitle:nil 
                                  otherButtonTitles:nil];
    
    actionSheet.destructiveButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString(@"Nueva tasación", @"Nueva tasación")];

    for (int i = 0; i < [buttonsArray count] ; i++) {
        [actionSheet addButtonWithTitle:[buttonsArray objectAtIndex:i]];
    }
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancelar", @"Cancelar")];
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    UIViewController *viewController = nil;
    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        switch (buttonIndex) {
            case 0:
                [self nuevaTasacion];
                break;
                
            case 1:
                if (isProVersion) {
                    viewController = [[FetchRequestViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((FetchRequestViewController *)viewController).managedObjectContext = self.managedObjectContext;
                    ((FetchRequestViewController *)viewController).entityName = @"Marcas";
                    ((FetchRequestViewController *)viewController).orderKey = @"marca";
                    ((FetchRequestViewController *)viewController).titleNavBar = @"Marcas";
                }
                break;
                
            case 2: {
                if (isProVersion) {
                    viewController = [[TasacionesTableViewController alloc] initWithStyle:UITableViewStylePlain];
                    ((TasacionesTableViewController *)viewController).managedObjectContext = self.managedObjectContext;
                }
                break;
            }
            case 99:
                if (![self fechaDeMatriculacionEsCorrecta]) {
                    UIAlertView *alertView = [[UIAlertView alloc]  
                                              initWithTitle:nil 
                                              message:NSLocalizedString(@"La fecha de matriculación debe ser anterior al día actual", @"La fecha de matriculación debe ser anterior al día actual") 
                                              delegate:self
                                              cancelButtonTitle:@"Ok" 
                                              otherButtonTitles:nil];
                    alertView.tag = 2;
                    [alertView show];
                } else {
                    [self selectionHasChangedForObjectSelected:[NSIndexPath indexPathForRow:JMTableSecSearchingRowFechaMatriculacion inSection:JMTableSectionSearching]];
                }
                break;
            default:
                break;
        }
    }
    if (viewController) {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (NSString *)informaAntesDeLaFila:(NSInteger)row
{
    int iMax = row;
    if (JMTableSecMandatoryNumRows < iMax) {
        iMax = JMTableSecMandatoryNumRows;
    }
    for (int i = 0; i < iMax; i++) {
        if (!self.tasacion.marca && i == JMTableSecMandatoryRowMarca) {
            return NSLocalizedString(@"Debe informar antes una marca", @"Debe informar antes una marca");
        }
        if (!self.tasacion.modelo && i == JMTableSecMandatoryRowModelo) {
            return NSLocalizedString(@"Debe informar antes un modelo", @"Debe informar antes un modelo");
        }
        if (!self.tasacion.fechaMatriculacion && i == JMTableSecMandatoryRowFechaMatriculacion) {
            return NSLocalizedString(@"Debe informar antes la fecha de matriculación", @"Debe informar antes la fecha de matriculación");
        }
        if (!self.tasacion.combustible && i == JMTableSecMandatoryRowCombustible) {
            return NSLocalizedString(@"Debe informar antes el tipo de combustible", @"Debe informar antes el tipo de combustible");
        }
    }
    return nil;
}

- (NSString *)validaObligatorios
{
    for (int i = 0; i < JMTableSecMandatoryNumRows; i++) {
        if (!self.tasacion.marca && i == JMTableSecMandatoryRowMarca) {
            return NSLocalizedString(@"Debe informar antes una marca", @"Debe informar antes una marca");
        }
        if (!self.tasacion.modelo && i == JMTableSecMandatoryRowModelo) {
            return NSLocalizedString(@"Debe informar antes un modelo", @"Debe informar antes un modelo");
        }
        if (!self.tasacion.fechaMatriculacion && i == JMTableSecMandatoryRowFechaMatriculacion) {
            return NSLocalizedString(@"Debe informar antes la fecha de matriculación", @"Debe informar antes la fecha de matriculación");
        }
        if (!self.tasacion.combustible && i == JMTableSecMandatoryRowCombustible) {
            return NSLocalizedString(@"Debe informar antes el tipo de combustible", @"Debe informar antes el tipo de combustible");
        }
    }
    return nil;
}

- (BOOL)avisoDeLimpiezaPorCambioEnFila:(NSInteger)row
{
    BOOL debeAvisar = NO;
    for (int i = row + 1; i < JMTableSecSearchingNumRows; i++) {
        if ((self.tasacion.marca && i == JMTableSecSearchingRowMarca) ||
            (self.tasacion.modelo && i == JMTableSecSearchingRowModelo) ||
            (self.tasacion.fechaMatriculacion && i == JMTableSecSearchingRowFechaMatriculacion) ||
            (self.tasacion.combustible && i == JMTableSecSearchingRowCombustible) ||
            (self.tasacion.cubicaje && i == JMTableSecSearchingRowCilindrada) ||
            (self.tasacion.potencia && i == JMTableSecSearchingRowPotencia)) {
            debeAvisar = YES;
        }
    }
    return debeAvisar;
}

- (void)selectionHasChangedForObjectSelected:(NSIndexPath *)indexPath
{
    [self.navigationController popViewControllerAnimated:YES];
    NSMutableArray *mutableFetchResults = nil;
    if (indexPath.section == 0) {
        int iMin = indexPath.row + 1;
        for (int i =  JMTableSecSearchingNumRows - 1; i > indexPath.row ; i--) {
//            if (self.tasacion.marca && i == JMTableSecSearchingRowMarca) {
//                self.tasacion.marca = nil;
//            }
            if (self.tasacion.modelo && i == JMTableSecSearchingRowModelo) {
                mutableFetchResults = [CoreDataHelper getObjectsFromContext:@"Modelos" withPredicate:[self predicateForModelComprobation] inManagedObjectContext:self.managedObjectContext];
                if ([mutableFetchResults count] == 0) {
                    self.tasacion.modelo = nil;
                }
            }
//            if (self.tasacion.fechaMatriculacion && i == JMTableSecSearchingRowFechaMatriculacion) {
//                self.tasacion.fechaMatriculacion = nil;
//            }
            if (self.tasacion.combustible && i == JMTableSecSearchingRowCombustible) {
                mutableFetchResults = [CoreDataHelper getObjectsFromContext:@"Acabados" withPredicate:[self predicateForAcabadosWithRow:[NSIndexPath indexPathForRow:JMTableSecSubmitRowSubmits inSection:JMTableSectionSubmit]] inManagedObjectContext:self.managedObjectContext];
                if ([mutableFetchResults count] == 0) {
                    self.tasacion.combustible = nil;
                }
            }
            if (self.tasacion.cubicaje && i == JMTableSecSearchingRowCilindrada) {
                mutableFetchResults = [CoreDataHelper getObjectsFromContext:@"Acabados" withPredicate:[self predicateForAcabadosWithRow:[NSIndexPath indexPathForRow:JMTableSecSubmitRowSubmits inSection:JMTableSectionSubmit]] inManagedObjectContext:self.managedObjectContext];
                if ([mutableFetchResults count] == 0) {
                    self.tasacion.cubicaje = nil;
                }
            }
            if (self.tasacion.potencia && i == JMTableSecSearchingRowPotencia) {
                mutableFetchResults = [CoreDataHelper getObjectsFromContext:@"Acabados" withPredicate:[self predicateForAcabadosWithRow:[NSIndexPath indexPathForRow:JMTableSecSubmitRowSubmits inSection:JMTableSectionSubmit]] inManagedObjectContext:self.managedObjectContext];
                if ([mutableFetchResults count] == 0) {
                    self.tasacion.potencia = nil;
                }
            }
        }
    }
}

- (void)nuevaTasacion
{
    [self.managedObjectContext deleteObject:self.tasacion];
    self.tasacion = [NSEntityDescription insertNewObjectForEntityForName:@"Tasaciones" inManagedObjectContext:managedObjectContext];
    [self.tableView reloadData];
}

- (void)isAboutToShowTheList
{
    [HUD hide:YES];
}

#pragma mark DatePickerView

UIDatePicker *theDatePicker;
UIToolbar* pickerToolbar;
UIActionSheet* pickerViewDate;

-(void)DatePickerView
{
    pickerViewDate = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Fecha de primera matriculación", @"Fecha de primera matriculación")
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:nil];
    
    theDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    theDatePicker.datePickerMode = UIDatePickerModeDate;
    theDatePicker.date = [NSDate date];
    if (self.tasacion.fechaMatriculacion) {
        theDatePicker.date = self.tasacion.fechaMatriculacion;
    } else {
        theDatePicker.date = [NSDate date];
    }
    
    //[theDatePicker release];
    [theDatePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle=UIBarStyleBlackTranslucent;
    [pickerToolbar sizeToFit];   
    NSMutableArray *barItems = [[NSMutableArray alloc] init];   
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DatePickerDoneClick)];
    [barItems addObject:flexSpace];
    [barItems addObject:doneButton];
    
    [pickerToolbar setItems:barItems animated:YES];       
    [pickerViewDate addSubview:pickerToolbar];
    [pickerViewDate addSubview:theDatePicker];
    [pickerViewDate  showInView:self.view];
    [pickerViewDate setBounds:CGRectMake(0,0,320, 464)];
}

-(IBAction)dateChanged{
    
    self.tasacion.fechaMatriculacion = theDatePicker.date;
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:JMTableSecSearchingRowFechaMatriculacion inSection:JMTableSectionSearching];
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	((JMCellStyleValue1 *)cell).detailTextLabel.text = [self.dateFormatter stringFromDate:self.tasacion.fechaMatriculacion];
    NSArray *arrayIndexPath = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:arrayIndexPath withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];

}


-(BOOL)closeDatePicker:(id)sender{   
    [pickerViewDate dismissWithClickedButtonIndex:99 animated:YES];
//    [self.tasacion.fechaMatriculacion resignFirstResponder];   
    return YES;
}

-(IBAction)DatePickerDoneClick{   
    [self closeDatePicker:self];
//    tableview.frame=CGRectMake(0, 44, 320, 416);
    
}


#pragma mark -
#pragma mark IBActions

- (IBAction)showSimple:(id)sender {
    // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
	
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
	
    // Show the HUD while the provided method executes in a new thread
    [HUD show:YES];
//    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}

- (IBAction)hideSimple:(id)sender
{
    [HUD hide:YES];
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
//    [HUD release];
	HUD = nil;
}

#pragma mark -
#pragma mark Banners:

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error 
{
    NSLog(@"iad failed");
    [self.bannerView removeFromSuperview];
    
    self.admobBannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 366, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height)];
    self.admobBannerView.adUnitID = @"a14f305b3728aff";
    self.admobBannerView.rootViewController = self;
    self.admobBannerView.delegate = self;
    
    // 4
    [self.view addSubview:self.admobBannerView];
    [self.admobBannerView loadRequest:[GADRequest request]];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error 
{
    NSLog(@"admob failed too");
    [self.admobBannerView removeFromSuperview];
}

@end
