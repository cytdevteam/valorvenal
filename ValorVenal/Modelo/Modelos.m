//
//  Modelos.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Modelos.h"
#import "Acabados.h"
#import "Marcas.h"


@implementation Modelos

@dynamic idModelo;
@dynamic modelo;
@dynamic acabados;
@dynamic marca;
@dynamic tasacion;

@end
