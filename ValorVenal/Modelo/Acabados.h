//
//  Acabados.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Marcas, Modelos;

@interface Acabados : NSManagedObject

@property (nonatomic, retain) NSString * acabado;
@property (nonatomic, retain) NSString * caballosFiscales;
@property (nonatomic, retain) NSString * caballosVapor;
@property (nonatomic, retain) NSString * cilindros;
@property (nonatomic, retain) NSString * cubicaje;
@property (nonatomic, retain) NSString * emisionesCO2;
@property (nonatomic, retain) NSString * idAcabado;
@property (nonatomic, retain) NSString * periodoComercial;
@property (nonatomic, retain) NSString * potenciaKWH;
@property (nonatomic, retain) NSString * tipoCombustible;
@property (nonatomic, retain) NSNumber * valorAcabado;
@property (nonatomic, retain) NSNumber * ventaDesde;
@property (nonatomic, retain) NSNumber * ventaHasta;
@property (nonatomic, retain) Marcas *marca;
@property (nonatomic, retain) NSSet *modelos;
@property (nonatomic, retain) NSSet *tasacion;
@end

@interface Acabados (CoreDataGeneratedAccessors)

- (void)addModelosObject:(Modelos *)value;
- (void)removeModelosObject:(Modelos *)value;
- (void)addModelos:(NSSet *)values;
- (void)removeModelos:(NSSet *)values;

- (void)addTasacionObject:(NSManagedObject *)value;
- (void)removeTasacionObject:(NSManagedObject *)value;
- (void)addTasacion:(NSSet *)values;
- (void)removeTasacion:(NSSet *)values;

@end
