//
//  Matriculas.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Matriculas : NSManagedObject

@property (nonatomic, retain) NSDate * fechaCorte;
@property (nonatomic, retain) NSString * letras;
@property (nonatomic, retain) NSNumber * numeros;
@property (nonatomic, retain) NSString * tipoMatricula;
@property (nonatomic, retain) NSString * zona;

@end
