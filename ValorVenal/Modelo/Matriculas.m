//
//  Matriculas.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Matriculas.h"


@implementation Matriculas

@dynamic fechaCorte;
@dynamic letras;
@dynamic numeros;
@dynamic tipoMatricula;
@dynamic zona;

@end
