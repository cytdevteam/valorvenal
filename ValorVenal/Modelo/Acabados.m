//
//  Acabados.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Acabados.h"
#import "Marcas.h"
#import "Modelos.h"


@implementation Acabados

@dynamic acabado;
@dynamic caballosFiscales;
@dynamic caballosVapor;
@dynamic cilindros;
@dynamic cubicaje;
@dynamic emisionesCO2;
@dynamic idAcabado;
@dynamic periodoComercial;
@dynamic potenciaKWH;
@dynamic tipoCombustible;
@dynamic valorAcabado;
@dynamic ventaDesde;
@dynamic ventaHasta;
@dynamic marca;
@dynamic modelos;
@dynamic tasacion;

@end
