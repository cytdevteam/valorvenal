//
//  Tasaciones.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 07/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Tasaciones.h"
#import "Acabados.h"
#import "Marcas.h"
#import "Modelos.h"


@implementation Tasaciones

@dynamic combustible;
@dynamic cubicaje;
@dynamic fecha;
@dynamic fechaMatriculacion;
@dynamic idTasacion;
@dynamic matricula;
@dynamic nombre;
@dynamic potencia;
@dynamic precio;
@dynamic acabado;
@dynamic marca;
@dynamic modelo;

- (NSNumber *)antiguedad 
{
    if (!self.fecha) {
        self.fecha = [NSDate date];
    }
    NSTimeInterval timeInterval = [self.fechaMatriculacion timeIntervalSinceDate:self.fecha];
    
    // Get the system calendar
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    
    // Create the NSDates
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] initWithTimeInterval:timeInterval sinceDate:date1]; 
    
    // Get conversion to months, days, hours, minutes
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit;
    
    NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
    
    NSLog(@"Break down: %dmin %dhours %ddays %dmonths %dyear",[breakdownInfo minute], [breakdownInfo hour], [breakdownInfo day], [breakdownInfo month], [breakdownInfo year]);
    
//    int antiguedad = -1 * timeInterval / (3600 * 24 * 365);
    int antiguedad = -1 * [breakdownInfo year];
    
    return [NSNumber numberWithInt:antiguedad];
}


@end
