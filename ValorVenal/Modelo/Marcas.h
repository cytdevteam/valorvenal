//
//  Marcas.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Acabados, Modelos, Tasaciones;

@interface Marcas : NSManagedObject

@property (nonatomic, retain) NSString * idMarca;
@property (nonatomic, retain) NSString * marca;
@property (nonatomic, retain) NSSet *acabados;
@property (nonatomic, retain) NSSet *modelos;
@property (nonatomic, retain) NSSet *tasacion;
@end

@interface Marcas (CoreDataGeneratedAccessors)

- (void)addAcabadosObject:(Acabados *)value;
- (void)removeAcabadosObject:(Acabados *)value;
- (void)addAcabados:(NSSet *)values;
- (void)removeAcabados:(NSSet *)values;

- (void)addModelosObject:(Modelos *)value;
- (void)removeModelosObject:(Modelos *)value;
- (void)addModelos:(NSSet *)values;
- (void)removeModelos:(NSSet *)values;

- (void)addTasacionObject:(Tasaciones *)value;
- (void)removeTasacionObject:(Tasaciones *)value;
- (void)addTasacion:(NSSet *)values;
- (void)removeTasacion:(NSSet *)values;

@end
