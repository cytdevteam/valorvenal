//
//  Tasaciones.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 07/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Acabados, Marcas, Modelos;

@interface Tasaciones : NSManagedObject

@property (nonatomic, retain) NSString * combustible;
@property (nonatomic, retain) NSString * cubicaje;
@property (nonatomic, retain) NSDate * fecha;
@property (nonatomic, retain) NSDate * fechaMatriculacion;
@property (nonatomic, retain) NSString * idTasacion;
@property (nonatomic, retain) NSString * matricula;
@property (nonatomic, retain) NSString * nombre;
@property (nonatomic, retain) NSString * potencia;
@property (nonatomic, retain) NSNumber * precio;
@property (nonatomic, retain) Acabados *acabado;
@property (nonatomic, retain) Marcas *marca;
@property (nonatomic, retain) Modelos *modelo;

- (NSNumber *)antiguedad;

@end
