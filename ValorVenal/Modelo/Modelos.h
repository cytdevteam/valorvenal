//
//  Modelos.h
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 03/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Acabados, Marcas;

@interface Modelos : NSManagedObject

@property (nonatomic, retain) NSString * idModelo;
@property (nonatomic, retain) NSString * modelo;
@property (nonatomic, retain) NSSet *acabados;
@property (nonatomic, retain) Marcas *marca;
@property (nonatomic, retain) NSSet *tasacion;
@end

@interface Modelos (CoreDataGeneratedAccessors)

- (void)addAcabadosObject:(Acabados *)value;
- (void)removeAcabadosObject:(Acabados *)value;
- (void)addAcabados:(NSSet *)values;
- (void)removeAcabados:(NSSet *)values;

- (void)addTasacionObject:(NSManagedObject *)value;
- (void)removeTasacionObject:(NSManagedObject *)value;
- (void)addTasacion:(NSSet *)values;
- (void)removeTasacion:(NSSet *)values;

@end
