//
//  Marcas.m
//  ValorVenal
//
//  Created by Javier Moreno Lozano on 20/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Marcas.h"
#import "Acabados.h"
#import "Modelos.h"
#import "Tasaciones.h"


@implementation Marcas

@dynamic idMarca;
@dynamic marca;
@dynamic acabados;
@dynamic modelos;
@dynamic tasacion;

@end
